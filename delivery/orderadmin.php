<?php
/**
 * Created by PhpStorm.
 * User: tasselchof
 * Date: 13.09.15
 * Time: 23:51
 */

namespace Sale\Handlers\Delivery;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Data\Cache;
use Bitrix\Main\Event;
use Bitrix\Main\EventManager;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\SystemException;
//use Bitrix\Main\Text\String;
use Bitrix\Main\SiteTable;
use Bitrix\Main\Web\HttpClient;
use Bitrix\Main\Diag\Debug;
use Bitrix\Orderadmin\Api;
use Bitrix\Sale\BasketItem;
use Bitrix\Sale\Delivery\ExtraServices\Store;
use Bitrix\Sale\Delivery\Services\Manager;
use Bitrix\Sale\Order;

Loc::loadMessages(__FILE__);

define('MODULE_NAME', 'iqcreative.orderadmin');
define('MODULE_VERSION', '0.0.10');
define('MODULE_CACHE_PERIOD', 60 * 60 * 24 * 7);

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/' . MODULE_NAME
    . '/include.php');

class OrderadminHandler extends \Bitrix\Sale\Delivery\Services\Base
{
    protected static $isCalculatePriceImmediately = true;
    protected static $whetherAdminExtraServicesShow = false;
    protected static $canHasProfiles = true;

    protected $api;
    protected $rates;

    public $locationFlag = false;

    public function __construct(array $initParams)
    {
        Asset::getInstance()->addCss(
            "https://unpkg.com/leaflet@1.0.3/dist/leaflet.css"
        );
        Asset::getInstance()->addCss(
            "/bitrix/modules/" . MODULE_NAME
            . "/plugins/Leaflet.markercluster/dist/MarkerCluster.css"
        );
        Asset::getInstance()->addCss(
            "/bitrix/modules/" . MODULE_NAME
            . "/plugins/Leaflet.markercluster/dist/MarkerCluster.Default.css"
        );
        Asset::getInstance()->addCss(
            "/bitrix/modules/" . MODULE_NAME . "/assets/css/map.css"
        );

        Asset::getInstance()->addJs(
            "https://unpkg.com/leaflet@1.0.3/dist/leaflet.js"
        );
        Asset::getInstance()->addJs(
            "/bitrix/modules/" . MODULE_NAME
            . "/plugins/Leaflet.markercluster/dist/leaflet.markercluster.js"
        );
        Asset::getInstance()->addJs(
            "/bitrix/modules/" . MODULE_NAME . "/assets/js/map.js"
        );

        $publicKey = Option::get(
            MODULE_NAME, self::getPrefix() . 'ORDERADMIN_PUBLIC_KEY'
        );
        $secret = Option::get(
            MODULE_NAME, self::getPrefix() . 'ORDERADMIN_SECRET'
        );
        $allowedDeliveryServices = array_filter(
            unserialize(
                Option::get(
                    MODULE_NAME,
                    self::getPrefix() . 'ORDERADMIN_DELIVERY_SERVICES'
                )
            )
        );

        $this->api = new Api($publicKey, $secret);

        if (empty($initParams['LOGOTIP'])) {
            $initParams['LOGOTIP'] = 'http://reworker.ru/images/logo.jpg';
        }

        //$initParams["EXTRA_SERVICES"] = $this->getEmbeddedExtraServicesList();

        /*
        if(isset($initParams["EXTRA_SERVICES"]))
            $this->extraServices = new \Bitrix\Sale\Delivery\ExtraServices\Manager($this->getEmbeddedExtraServicesList(), $this->currency);
        elseif($this->id > 0)
            $this->extraServices = new \Bitrix\Sale\Delivery\ExtraServices\Manager($this->id, $this->currency);
        else
            $this->extraServices = new \Bitrix\Sale\Delivery\ExtraServices\Manager(array(), $this->currency);
        */

        //$initParams["EXTRA_SERVICES"] = $this->getEmbeddedExtraServicesList();

        parent::__construct($initParams);
    }

    public static function getClassTitle()
    {
        return 'Orderadmin';
    }

    public static function getClassDescription()
    {
        return 'Службы доставки reWorker';
    }

    public function isCalculatePriceImmediately()
    {
        return self::$isCalculatePriceImmediately;
    }

    public static function whetherAdminExtraServicesShow()
    {
        return self::$whetherAdminExtraServicesShow;
    }

    public static function canHasProfiles()
    {
        return self::$canHasProfiles;
    }

    public static function getChildrenClassNames()
    {
        return array(
            OrderadminServicepointHandler::class,
            OrderadminCourierHandler::class,
        );
    }

    public function getProfilesList()
    {
        if (empty($this->config['MAIN']['DELIVERY_SERVICE'])) {
            return array();
        }

        // Building list of available rates
        $res = $this->getApi()->request(
            HttpClient::HTTP_GET, '/api/delivery-services/rates',
            array('deliveryService' => $this->config['MAIN']['DELIVERY_SERVICE'])
        )->getResult(true);

        $ratesList = array();
        foreach ($res['_embedded']['rates'] as $rate) {
            if ($rate['state'] != 'active') {
                continue;
            }

            $ratesList[$rate['id']] = $rate['name'];

            $this->rates[$rate['id']] = $rate;
        }

        return $ratesList;
    }

    public function getProfilesDefaultParams(array $fields = array())
    {
        $result = array();

        // Building list of available rates
        $res = $this->getApi()->request(
            HttpClient::HTTP_GET, '/api/delivery-services/rates',
            array(
                'filter' => array(
                    array(
                        'type'  => 'eq',
                        'field' => 'state',
                        'value' => 'active',
                    ),
                    array(
                        'type'  => 'eq',
                        'field' => 'deliveryService',
                        'value' => $this->config['MAIN']['DELIVERY_SERVICE'],
                    ),
                ),
            )
        )->getResult(true);

        foreach ($res['_embedded']['rates'] as $rate) {
            $this->rates[$rate['id']] = $rate;
        }

        foreach ($this->rates as $rateId => $params) {
            $comment = !empty($params["comment"]) ? $params["comment"] : "";

            if (in_array(
                $params['type'], ['service_point', 'self_service_point']
            )
            ) {
                $comment .= ' ';
            }

            $logo = '';
            if (!empty($params['_embedded']['deliveryService']['logo'])) {
                $endpoint = Option::get(MODULE_NAME, 'ORDERADMIN_API_URL');

                $logo = str_replace('/api/rest/latest', '', $endpoint)
                    . $params['_embedded']['deliveryService']['logo'];
            }

            $result[] = array(
                "CODE"        => $rateId,
                "PARENT_ID"   => $this->id,
                "NAME"        => $params["name"],
                "ACTIVE"      => $params["state"] == 'active' ? "Y" : "N",
                "SORT"        => $this->sort,
                "LOGOTIP"     => $logo,
                "DESCRIPTION" => $comment,
                "CLASS_NAME"  => in_array(
                    $params['type'], ['courier', 'russian_post']
                ) ? OrderadminCourierHandler::class
                    : OrderadminServicepointHandler::class,
                "CURRENCY"    => $this->currency,
                "CONFIG"      => array(
                    "MAIN" => array(
                        "RATE_ID" => $rateId,
                    )
                )
            );
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getEmbeddedExtraServicesList()
    {
        return self::getAllExtraServices();
    }

    public static function getAllExtraServices()
    {
        return array(
            "SMS" => array(
                "NAME"        => 'Склад отгрузки',
                "SORT"        => 100,
                "RIGHTS"      => "NYN",
                "ACTIVE"      => "Y",
                "CLASS_NAME"  => Store::class,
                "DESCRIPTION" => 'Выбрать склад, с которого будет выдан заказ',
                "INIT_VALUE"  => "N",
                "PARAMS"      => array("PRICE" => 0)
            ),
        );
    }

    public static function getAdminFieldsList()
    {
        $result = parent::getAdminFieldsList();
        $result["STORES"] = true;
        return $result;
    }

    public static function onAfterAdd($serviceId, array $fields = array())
    {
        if ($serviceId <= 0) {
            return false;
        }

        $fields["ID"] = $serviceId;
        $srv = new self($fields);
        $profiles = $srv->getProfilesDefaultParams($fields);

        if (!is_array($profiles)) {
            return false;
        }

        $result = true;

        foreach ($profiles as $profile) {
            $res = Manager::add($profile);
            $result = $result && $res->isSuccess();
        }

        return $result;
    }

    /**
     * @return Api
     */
    private function getApi()
    {
        return $this->api->setRequest(array());
    }

    private static function getPrefix()
    {
        $rsSite = SiteTable::getById(SITE_ID);
        $arSite = $rsSite->Fetch();

        $prefix = null;
        if ($arSite['DEF'] != 'Y' && !is_null($arSite['LID'])) {
            $prefix = strtoupper($arSite['LID']) . '_';
        }

        return $prefix;
    }

    protected function getConfigStructure()
    {
        // Building list of delivery services
        $res = $this->getApi()->request(
            HttpClient::HTTP_GET, '/api/delivery-services',
            array(
                'filter' => array(
                    array(
                        'type'  => 'eq',
                        'field' => 'state',
                        'value' => 'active', 2 => 'filter',
                        3       => 'order-by',
                    ),
                ),
            )
        )->getResult(true);
        $deliveryServicesList = array();
        foreach ($res['_embedded']['delivery_services'] as $deliveryService) {
            $deliveryServicesList[$deliveryService['id']]
                = $deliveryService['name'];
        }

        $result = array(
            'MAIN' => array(
                'TITLE'       => 'Основные',
                'DESCRIPTION' => 'Основные настройки',
                'ITEMS'       => array(
                    'DELIVERY_SERVICE' => array(
                        'TYPE'    => 'ENUM',
                        'NAME'    => 'Служба доставки',
                        'DEFAULT' => '',
                        'OPTIONS' => $deliveryServicesList,
                    ),
                )
            )
        );

        return $result;
    }

    private function getLocalityByCode($locationCode)
    {
        if (empty($locationCode)) {
            throw new \Bitrix\Main\SystemException(
                'Locality not found (location code is empty)'
            );
        }

        $cacheKeys = array(
            'MODULE'  => MODULE_NAME,
            'VERSION' => MODULE_VERSION,
            'SECTION' => 'locality',
            'ID'      => $locationCode,
        );

        $obCache = Cache::createInstance();
        $cache = $obCache->initCache(
            MODULE_CACHE_PERIOD, join($cacheKeys), '/'
        );

        if ($cache) {
            $vars = $obCache->getVars();
            $locality = $vars["RESULT"];
        } else {
            $loc = \Bitrix\Sale\Location\LocationTable::getByCode(
                $locationCode, array(
                    'filter' => array('=NAME.LANGUAGE_ID' => LANGUAGE_ID),
                    'select' => array('*', 'NAME_RU' => 'NAME.NAME')
                )
            )->Fetch();

            $res = $this->getApi()->request(
                HttpClient::HTTP_GET, '/api/locations/localities', array(
                    'filter' => array(
                        array(
                            'type'  => 'eq',
                            'field' => 'name',
                            'value' => $loc['NAME_RU'],
                        ),
                        array(
                            'type'   => 'in',
                            'field'  => 'type',
                            'values' => array(
                                'Город',
                            ),
                        ),
                    ),
                )
            )->getResult(true);

            if ($res['total_items'] == 1) {
                $locality = $res['_embedded']['localities'][0];
            } elseif ($res['total_items'] > 1) {
                $locality = null;
                foreach ($res['_embedded']['localities'] as $localityData) {
                    if ($localityData['type'] == 'Город') {
                        $locality = $localityData;
                    }
                }

                if (is_null($locality)) {
                    throw new \Bitrix\Main\SystemException(
                        sprintf(
                            'Locality %s not matched (%s localities found)',
                            $locationCode, $res['total_items']
                        )
                    );
                }
            } else {
                throw new \Bitrix\Main\SystemException(
                    sprintf(
                        'Locality %s (data from database: %s) not found: %s', $locationCode, var_export($loc, true),
                        var_export($res, true)
                    )
                );
            }

            $obCache->endDataCache(
                array(
                    "RESULT" => $locality,
                )
            );
        }

        return $locality;
    }

    protected function calculateConcrete(\Bitrix\Sale\Shipment $shipment = null)
    {
        $order = $shipment->getCollection()->getOrder(); // заказ
        $props = $order->getPropertyCollection();

        $locationCode = null;
        if (!empty($props->getDeliveryLocation())) {
            $locationCode = $props->getDeliveryLocation()->getValue(
            ); // местоположение
        }

        if (!empty($locationCode)) {
            $this->locationFlag = true;
        }

        try {
            $locality = $this->getLocalityByCode($locationCode);
            Debug::dumpToFile($locality, '', '__bx_log.log');

        } catch (SystemException $e) {
            Debug::dumpToFile($e->getMessage(), '', '__bx_log.log');

            return array();
        }

        $deliveryServices = array_filter(
            unserialize(
                Option::get(
                    MODULE_NAME,
                    self::getPrefix() . 'ORDERADMIN_DELIVERY_SERVICES'
                )
            )
        );
        $prepaymentDeliveryServices = array_filter(
            unserialize(
                Option::get(
                    MODULE_NAME, self::getPrefix()
                    . 'ORDERADMIN_PREPAYMENT_DELIVERY_SERVICES'
                )
            )
        );
        $prepaymentServices = array_filter(
            unserialize(
                Option::get(
                    MODULE_NAME,
                    self::getPrefix() . 'ORDERADMIN_PREPAYMENT_SERVICES'
                )
            )
        );

        $country = Option::get(
            MODULE_NAME, self::getPrefix() . 'ORDERADMIN_BASE_COUNTRY'
        );
        $baseWeight = Option::get(
            MODULE_NAME, self::getPrefix() . 'ORDERADMIN_BASE_WEIGHT'
        );
        $warehouse = Option::get(
            MODULE_NAME, self::getPrefix() . 'ORDERADMIN_WAREHOUSE'
        );
        $sender = Option::get(
            MODULE_NAME, self::getPrefix() . 'ORDERADMIN_SENDER'
        );

        $dimensions = array(
            'WIDTH'  => 0,
            'HEIGHT' => 0,
            'LENGTH' => 0,
        );

        /** @var Order $order */
        $order = $shipment->getCollection()->getOrder();

        /** @var BasketItem $basketItem */
        foreach ($order->getBasket()->getBasketItems() as $basketItem) {
            $basketItemDimensions = $basketItem->getField("DIMENSIONS");

            if (is_string($basketItemDimensions)) {
                $basketItemDimensions = unserialize($basketItemDimensions);
            }

            if (!is_array($basketItemDimensions)
                || empty($basketItemDimensions)
            ) {
                continue;
            }

            $dimensions['WIDTH'] += $basketItemDimensions['WIDTH'];
            $dimensions['HEIGHT'] += $basketItemDimensions['HEIGHT'];
            $dimensions['LENGTH'] += $basketItemDimensions['LENGTH'];
        }

        $payment = $order->getPrice();
        if (in_array($order->getPaymentSystemId(), $prepaymentServices)) {
            $payment = 0;
        }

        $cacheKeys = array(
            'MODULE'         => MODULE_NAME,
            'VERSION'        => MODULE_VERSION,
            'SECTION'        => 'delivery_services',
            'DESTINATION'    => $locality['id'],
            'WEIGHT'         => $shipment->getWeight(),
            'WIDTH'          => $dimensions['WIDTH'],
            'HEIGHT'         => $dimensions['HEIGHT'],
            'LENGTH'         => $dimensions['LENGTH'],
            'PAYMENT'        => $payment,
            'ESTIMATED_COST' => $order->getPrice(),
        );

        $obCache = Cache::createInstance();
        $cache = $obCache->initCache(
            MODULE_CACHE_PERIOD, join($cacheKeys), '/'
        );

        if ($cache) {
            $vars = $obCache->GetVars();
            $res = $vars["RESULT"];
        }

        Debug::dumpToFile($res, '', '__bx_log.log');

        if (
            !$cache || empty($res) && !empty($locality)
        ) {
            if (array_search(
                $order->getPaymentSystemId(), $prepaymentServices
            )
            ) {
                $deliveryServicesArr = $prepaymentDeliveryServices;
            } else {
                $deliveryServicesArr = $deliveryServices;
            }

            $deliveryServices = array();
            if (!empty($deliveryServicesArr)) {
                foreach ($deliveryServicesArr as $deliveryService) {
                    $deliveryServices[] = array(
                        'id' => $deliveryService,
                    );
                }
            }

            $date = new \DateTime('tomorrow');

            $request = array(
                'debug'             => false,
                'delivery-services' => $deliveryServices,
                'sender'            => $sender,
                'to'                => array(
                    'id' => $locality['id'],
                ),
                "date"              => $date->format('Y-m-d'),
                "weight"            => !empty($shipment->getWeight())
                    ? $shipment->getWeight() : $baseWeight,
                "width"             => $dimensions['WIDTH'],
                "height"            => $dimensions['HEIGHT'],
                "length"            => $dimensions['LENGTH'],
                "estimatedCost"     => $order->getPrice(),
                "payment"           => $payment,
            );

            Debug::dumpToFile(
                json_encode($request), '', '__bx_calculation.log'
            );

            $res = $this->getApi()->setRequest($request)->request(
                HttpClient::HTTP_POST, '/api/delivery-services/calculator'
            )->getResult(true);

//            echo '<pre>';
//            echo json_encode($request, JSON_PRETTY_PRINT + JSON_UNESCAPED_UNICODE);
//            echo '</pre>';
//            echo '<pre>';
//            echo json_encode($this->getApi()->getError(true), JSON_PRETTY_PRINT + JSON_UNESCAPED_UNICODE);
//            echo '</pre>';
//            die();

            Debug::dumpToFile(
                $this->getApi()->getError(), '', '__bx_calculation.log'
            );

//            Debug::dumpToFile($res, '', '__bx_calculation.log');

            if (!empty($res)) {
                if (!empty($res) && $obCache->startDataCache()) {
                    $obCache->endDataCache(
                        array(
                            "RESULT" => $res,
                        )
                    );
                }
            } else {
                $res = json_decode($this->getApi()->getError(), true);
            }
        }

        $_SESSION['oaCurrentCalculation'] = $res;

        return $res;
    }
}

/*
EventManager::getInstance()->addEventHandler("sale", "OnSaleComponentOrderOneStepDelivery",
    function(&$arResult, &$arUserResult, &$arParams) {
        foreach ($arResult['DELIVERY'] as &$arDelivery) {
            $servicePointChooser = '<input type="hidden" name="oaServicePoint" value="" />';

            $arDelivery['DESCRIPTION'] .= $servicePointChooser;
        }
    }
);
*/

//Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/styles/fix.css");

$rsProps = \Bitrix\Sale\Internals\OrderPropsTable::getList(
    array(
        'filter' => array(
            '=CODE' => 'servicePoint',
        ),
    )
);

$servicePointProp = $rsProps->Fetch();

$dbSubServicesRes = \Bitrix\Sale\Delivery\Services\Table::getList(
    array(
        "filter" => array(),
    )
);

$rates = array();
while ($arProfile = $dbSubServicesRes->Fetch()) {
    if (!empty($arProfile['CONFIG']['MAIN']['RATE_ID'])) {
        $rates[$arProfile['CONFIG']['MAIN']['RATE_ID']] = $arProfile['ID'];
    }
}

Asset::getInstance()->addString(
    '<script type="text/javascript">var oaServicePointProp = ' . json_encode(
        $servicePointProp
    ) . ';var oaRates = ' . json_encode($rates) . ';</script>'
);
