<?php
/**
 * Created by PhpStorm.
 * User: tasselchof
 * Date: 13.09.15
 * Time: 23:51
 */

use Bitrix\Main\Localization\Loc;

//use Bitrix\Main\Text\String;

Loc::loadMessages(__FILE__);

/**
 *
 */
define('MODULE_NAME', 'iqcreative.orderadmin');
/**
 *
 */
define('MODULE_VERSION', '0.0.3');
/**
 *
 */
define('MODULE_CACHE_PERIOD', 60 * 60 * 24 * 7);

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/' . MODULE_NAME
    . '/include.php';

/**
 * Class CDeliveryOrderadmin
 * @deprecated
 */
class CDeliveryOrderadmin
{
    /**
     * @return array
     */
    public function Init()
    {
        $base_currency = 'RUB';

        if ($arCurrency = CCurrency::GetByID('RUR')) {
            $base_currency = 'RUR';
        }

        $config = [
            'SID'               => 'orderadmin',
            'NAME'              => 'Orderadmin',
            'DESCRIPTION'       => GetMessage(
                'IQCREATIVE_ORDERADMIN_SLUJBY_DOSTAVKI'),
            'DESCRIPTION_INNER' => GetMessage(
                'IQCREATIVE_ORDERADMIN_SLUJBY_DOSTAVKI'),
            'BASE_CURRENCY'     => $base_currency,
            'HANDLER'           => __FILE__,

            'DBGETSETTINGS' => [__CLASS__, 'GetSettings'],
            'DBSETSETTINGS' => [__CLASS__, 'SetSettings'],
            'COMPABILITY'   => [__CLASS__, 'Compability'],
            'CALCULATOR'    => [__CLASS__, 'Calculate'],

            'PROFILES' => [
                "courier" => array(
                    "TITLE" => 'Курьерская доставка (reWorker)',
                    "DESCRIPTION" => 'Доставка курьерской службой',
                    "RESTRICTIONS_WEIGHT" => array(0),
                    "RESTRICTIONS_SUM" => array(0),
                ),
                "service_point" => array(
                    "TITLE" => 'Доставка до ПВЗ (reWorker)',
                    "DESCRIPTION" => 'Доставка до пункта выдачи заказов',
                    "RESTRICTIONS_WEIGHT" => array(0),
                    "RESTRICTIONS_SUM" => array(0),
                ),
                "self_service_point" => array(
                    "TITLE" => 'Доставка до постамата (reWorker)',
                    "DESCRIPTION" => '',
                    "RESTRICTIONS_WEIGHT" => array(0),
                    "RESTRICTIONS_SUM" => array(0),
                ),
                "russian_post" => array(
                    "TITLE" => 'Доставка Почтой России (reWorker)',
                    "DESCRIPTION" => '',
                    "RESTRICTIONS_WEIGHT" => array(0),
                    "RESTRICTIONS_SUM" => array(0),
                ),

            ]
        ];

        $publicKey = COption::GetOptionString(
            MODULE_NAME, self::getPrefix() . 'ORDERADMIN_PUBLIC_KEY');
        $secret = COption::GetOptionString(
            MODULE_NAME, self::getPrefix() . 'ORDERADMIN_SECRET');
        $allowedDeliveryServices = array_filter(
            unserialize(
                COption::GetOptionString(
                    MODULE_NAME,
                    self::getPrefix() . 'ORDERADMIN_DELIVERY_SERVICES')));

        $calc = new \Bitrix\Orderadmin\Api($publicKey, $secret);

        $res = $calc->setRequest([])->request(
            'GET', '/api/delivery-services')->getResult();
        $res = json_decode($res, true);

        $deliveryServices = [];
        foreach ($res['_embedded']['delivery_service'] as $deliveryService) {
            $deliveryServices[$deliveryService['id']] = $deliveryService;
        }

        $url = COption::GetOptionString(MODULE_NAME, 'ORDERADMIN_API_URL');
        $url = parse_url($url);
        $url = $url['scheme'] . '://' . $url['host'];

        $res = $calc->setRequest([])->request(
            'GET', '/delivery-services/rates?limit=100')->getResult();
        $rates = json_decode($res, true);

        foreach ($rates['result'] as $rate) {
            if (!in_array(
                $rate['deliveryService'], $allowedDeliveryServices)
            ) {
                continue;
            }

            $description = '<b>Служба доставки:</b> '
                . $deliveryServices[$rate['deliveryService']]['name'];

            $config['PROFILES'][$rate['id']] = [
                'TITLE'               => $rate['name'],
                'DESCRIPTION'         => $description,
                'LOGOTIP'             => $url
                    . $deliveryServices[$rate['deliveryService']]['logo'],
                'RESTRICTIONS_WEIGHT' => [0],
                'RESTRICTIONS_SUM'    => [0]
            ];
        }

        return $config;
    }

    /**
     * @return null|string
     */
    private static function getPrefix()
    {
        $rsSite = CSite::GetByID(SITE_ID);
        $arSite = $rsSite->Fetch();

        $prefix = null;
        if ('Y' !== $arSite['DEF'] && null !== $arSite['LID']) {
            $prefix = strtoupper($arSite['LID']) . '_';
        }

        return $prefix;
    }

    /* Установка параметров */

    /**
     * @param $arSettings
     *
     * @return string
     */
    public function SetSettings($arSettings)
    {
        return serialize($arSettings);
    }

    /* Запрос параметров */
    /**
     * @param $strSettings
     *
     * @return mixed
     */
    public function GetSettings($strSettings)
    {
        return unserialize($strSettings);
    }

    /* Запрос конфигурации службы доставки */
    /**
     * @return array
     */
    public function GetConfig()
    {
        $arConfig = [
            'CONFIG_GROUPS' => [
                'all' => GetMessage('IQCREATIVE_ORDERADMIN_PARAMETRY')
            ],

            'CONFIG' => [
                'DELIVERY_PRICE' => [
                    'TYPE'    => 'STRING',
                    'DEFAULT' => '200',
                    'TITLE'   => GetMessage(
                        'IQCREATIVE_ORDERADMIN_STOIMOSTQ_DOSTAVKI'),
                    'GROUP'   => 'all'
                ]
            ]
        ];

        return $arConfig;
    }

    /**
     * @param $arOrder
     * @param $arConfig
     *
     * @return array
     */
    public function Compability($arOrder, $arConfig)
    {
        $_SESSION['OrderadminCalculation']
            = self::_calculateDelivery($arOrder);

        $result = [];
        foreach ($_SESSION['OrderadminCalculation']['rates'] as $rate) {
            $result[] = $rate['id'];
        }

        return $result;
    }

    /**
     * @param $arOrder
     *
     * @return mixed
     */
    public function _calculateDelivery($arOrder)
    {
        $deliveryServices = array_filter(
            unserialize(
                COption::GetOptionString(
                    MODULE_NAME,
                    self::getPrefix() . 'ORDERADMIN_DELIVERY_SERVICES')));
        $prepaymentDeliveryServices = array_filter(
            unserialize(
                COption::GetOptionString(
                    MODULE_NAME, self::getPrefix()
                    . 'ORDERADMIN_PREPAYMENT_DELIVERY_SERVICES')));
        $prepaymentServices = array_filter(
            unserialize(
                COption::GetOptionString(
                    MODULE_NAME,
                    self::getPrefix() . 'ORDERADMIN_PREPAYMENT_SERVICES')));

        $publicKey = COption::GetOptionString(
            MODULE_NAME, self::getPrefix() . 'ORDERADMIN_PUBLIC_KEY');
        $secret = COption::GetOptionString(
            MODULE_NAME, self::getPrefix() . 'ORDERADMIN_SECRET');
        $country = COption::GetOptionString(
            MODULE_NAME, self::getPrefix() . 'ORDERADMIN_BASE_COUNTRY');
        $baseWeight = COption::GetOptionString(
            MODULE_NAME, self::getPrefix() . 'ORDERADMIN_BASE_WEIGHT');
        $warehouse = COption::GetOptionString(
            MODULE_NAME, self::getPrefix() . 'ORDERADMIN_WAREHOUSE');
        $sender = COption::GetOptionString(
            MODULE_NAME, self::getPrefix() . 'ORDERADMIN_SENDER');

        $arLocationTo = $arOrder['LOCATION_TO'];

        $dimentions = [
            'WIDTH'  => 0,
            'HEIGHT' => 0,
            'LENGTH' => 0
        ];

        foreach ($arOrder['ITEMS'] as $item) {
            $dimentions['WIDTH'] += $item['ITEMS_DIMENSIONS']['WIDTH'];
            $dimentions['HEIGHT'] += $item['ITEMS_DIMENSIONS']['HEIGHT'];
            $dimentions['LENGTH'] += $item['ITEMS_DIMENSIONS']['LENGTH'];
        }

        $payment = $arOrder['PRICE'];
        /*
        if ($arUserResult['PAY_SYSTEM_ID'] == 0 || !empty($prepaymentServices) && in_array($arUserResult['PAY_SYSTEM_ID'], $prepaymentServices)) {
            $payment = 0;
        }
        */

        $cacheKeys = [
            'MODULE'         => MODULE_NAME,
            'VERSION'        => MODULE_VERSION,
            'SECTION'        => 'delivery_services',
            'DESTINATION'    => $arLocationTo,
            'WEIGHT'         => $arOrder['WEIGHT'],
            'WIDTH'          => $dimentions['WIDTH'],
            'HEIGHT'         => $dimentions['HEIGHT'],
            'LENGTH'         => $dimentions['LENGTH'],
            'PAYMENT'        => $payment,
            'ESTIMATED_COST' => $arOrder['PRICE']
        ];

        $obCache = new CPHPCache();
        $cache = $obCache->InitCache(
            MODULE_CACHE_PERIOD, implode($cacheKeys), '/');

        if ($cache) {
            $vars = $obCache->GetVars();
            $res = $vars['RESULT'];
        } else {
            $calc = new \Bitrix\Orderadmin\Api($publicKey, $secret);

            $locality = self::_getLocalityByCity($arLocationTo);

            $paySystem = $arUserResult['PAY_SYSTEM_ID'];

            $deliveryServicesArr = $deliveryServices;

            if (in_array($paySystem, $prepaymentServices)) {
                $deliveryServicesArr = $prepaymentDeliveryServices;
            }

            $deliveryServices = [];
            if (!empty($deliveryServicesArr)) {
                foreach ($deliveryServicesArr as $deliveryService) {
                    $deliveryServices[] = [
                        'id' => $deliveryService
                    ];
                }
            }

            $date = new DateTime('tomorrow');

            $res = $calc->setRequest(
                [
                    'debug'             => false,
                    'delivery-services' => $deliveryServices,
                    'from'              => [
                        'sender' => $sender
                    ],
                    'to'                => [
                        'id' => $locality['id']
                    ],
                    'date'              => $date->format('Y-m-d'),
                    'weight'            => $arOrder['WEIGHT']
                        ?: $baseWeight,
                    'width'             => $dimentions['WIDTH'],
                    'height'            => $dimentions['HEIGHT'],
                    'length'            => $dimentions['LENGTH'],
                    'estimatedCost'     => $arOrder['PRICE'],
                    'payment'           => $payment
                ])->request('POST', '/delivery-services/calculator')
                ->getResult();

            if ($res) {
                $res = json_decode($res, true);

                if (!empty($res) && $obCache->StartDataCache()) {
                    $obCache->EndDataCache(
                        [
                            'RESULT' => $res
                        ]);
                }
            } else {
                $res = json_decode($calc->getError(), true);
            }
        }

        return $res;
    }

    /**
     * @param $location_id
     *
     * @return null
     */
    public function _getLocalityByCity($location_id)
    {
        if (empty($location_id)) {
            return false;
        }

        $city = CSaleLocation::GetByID($location_id);

        $cityName = $city['CITY_NAME_LANG'];
        if (LANG_CHARSET !== 'UTF-8') {
            $cityName = iconv(LANG_CHARSET, 'utf-8', $cityName);
        }

        $cacheKeys = [
            'MODULE'  => MODULE_NAME,
            'VERSION' => MODULE_VERSION,
            'SECTION' => 'locality',
            'ID'      => $location_id
        ];

        $obCache = new CPHPCache();
        $cache = $obCache->InitCache(
            MODULE_CACHE_PERIOD, implode($cacheKeys), '/');

        if ($cache) {
            $vars = $obCache->GetVars();
            $locality = $vars['RESULT'];
        } else {
            $publicKey = COption::GetOptionString(
                MODULE_NAME, self::getPrefix() . 'ORDERADMIN_PUBLIC_KEY');
            $secret = COption::GetOptionString(
                MODULE_NAME, self::getPrefix() . 'ORDERADMIN_SECRET');
            $country = COption::GetOptionString(
                MODULE_NAME, self::getPrefix() . 'ORDERADMIN_BASE_COUNTRY');

            $calc = new \Bitrix\Orderadmin\Api($publicKey, $secret);
            $res = $calc->request(
                'GET', '/api/locations/localities', array(
                'criteria' => array(
                    'country' => $country,
                    'name'    => $cityName,
                ),
            ));
            $res = json_decode($res->getResult(), true);

            if ($res['total_items'] == 1) {
                $locality = $res['_embedded']['localities'][0];
            } elseif ($res['total_items'] > 1) {
                $locality = null;
                foreach ($res['_embedded']['localities'] as $localityData) {
                    if ($localityData['type'] == 'Город') {
                        $locality = $localityData;
                    }
                }

                if (is_null($locality)) {
                    throw new \Bitrix\Main\SystemException(
                        sprintf(
                            'Locality %s not matched (%s localities found)',
                            $cityName, $res['total_items']
                        )
                    );
                }
            } else {
                throw new \Bitrix\Main\SystemException(
                    sprintf(
                        'Locality %s not found: %s', $cityName,
                        var_export($res, true)
                    )
                );
            }

            if (!empty($locality) && $obCache->StartDataCache()) {
                $obCache->EndDataCache(
                    [
                        'RESULT' => $locality
                    ]);
            }
        }

        return $locality;
    }

    /**
     * @param      $profile
     * @param      $arConfig
     * @param      $arOrder
     * @param      $STEP
     * @param bool $TEMP
     *
     * @return array
     */
    public function Calculate($profile, $arConfig, $arOrder, $STEP, $TEMP = false)
    {
        if (!$_SESSION['OrderadminCalculation']) {
            return [
                'RESULT' => 'ERROR',
                'TEXT'   => GetMessage('IQCREATIVE_ORDERADMIN_OSIBKA')
            ];
        }

        $result = null;
        foreach ($_SESSION['OrderadminCalculation']['rates'] as $rate) {
            if ($rate['id'] == $profile) {
                $result = $rate;
            }
        }

        if (null === $result) {
            return [
                'RESULT' => 'ERROR',
                'TEXT'   => 'Нет доставки данной курьерской службой'
            ];
        }

        return [
            'RESULT'  => 'OK',
            'TRANSIT' => $result['deliveryPeriodStr'],
            'VALUE'   => $result['price']
        ];
    }

    public function IncludeScripts()
    {
    }
}

//EventManager::getInstance()->addEventHandler('sale', 'onSaleDeliveryHandlersBuildList', array("CDeliveryOrderadmin", "Init"));
//EventManager::getInstance()->addEventHandler('main', 'OnEpilog', array("CDeliveryOrderadmin", "IncludeScripts"));
//EventManager::getInstance()->addEventHandler('sale', 'OnBeforeOrderAdd', array("CDeliveryOrderadmin", "UpdateDeliveryProfile"));
