<?php
/**
 * Created by PhpStorm.
 * User: tasselchof
 * Date: 13.09.15
 * Time: 23:51
 */

namespace Sale\Handlers\Delivery;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Diag\Debug;
use Bitrix\Main\Error;
use Bitrix\Main\EventManager;
use Bitrix\Main\Localization\Loc;
//use Bitrix\Main\Text\String;
use Bitrix\Main\SiteTable;
use Bitrix\Main\Web\HttpClient;
use Bitrix\Orderadmin\Api;
use Bitrix\Sale\Delivery\CalculationResult;
use Bitrix\Sale\Shipment;

Loc::loadMessages(__FILE__);

define('MODULE_NAME', 'iqcreative.orderadmin');
define('MODULE_VERSION', '0.0.5');
define('MODULE_CACHE_PERIOD', 60 * 60 * 24 * 7);

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/' . MODULE_NAME . '/include.php');

class OrderadminServicepointHandler extends \Bitrix\Sale\Delivery\Services\Base
{
    protected static $isProfile = true;
    protected static $parent = null;

    protected $rateId;

    public function __construct(array $initParams)
    {
        if (!$initParams['CONFIG']) {
            $initParams['CONFIG'] = array();
        }

        if (empty($initParams['CONFIG']['MAIN']['RATE_ID'])) {
            throw new \Bitrix\Main\SystemException('Rate not found');
        }

        $this->rateId = $initParams['CONFIG']['MAIN']['RATE_ID'];

        parent::__construct($initParams);

        $this->parent = \Bitrix\Sale\Delivery\Services\Manager::getObjectById($this->parentId);
    }

    public static function getClassTitle()
    {
        return 'Orderadmin service point';
    }

    public static function getClassDescription()
    {
        return 'Доставка до ПВЗ <a href="#" data-action="oa-load-map" data-toggle="modal" data-target="#oaServicePointsModal">Выбрать...</a>';
    }

    public function getParentService()
    {
        return $this->parent;
    }

    public function isCalculatePriceImmediately()
    {
        return $this->getParentService()->isCalculatePriceImmediately();
    }

    public static function isProfile()
    {
        return self::$isProfile;
    }

    protected function getConfigStructure()
    {
        return array(
            "MAIN" => array(
                'TITLE' => 'Основные',
                'DESCRIPTION' => 'Основные настройки',
                'ITEMS' => array(
                    'RATE_ID' => array(
                        "TYPE" => 'STRING',
                        "HIDDEN" => true,
                        "NAME" => 'Delivery service rate ID',
                        "DEFAULT" => $this->rateId,
                    ),
                )
            )
        );
    }

    protected function chooseRate($res)
    {
        if (empty($res['rates'])) {
            return false;
        }

        $rate = null;
        foreach ($res['rates'] as $result) {
            if ($this->rateId == $result['id']) {
                $rate = $result;
            }
        }

        return $rate;
    }

    protected function calculateConcrete(Shipment $shipment = null)
    {
        $res = $this->getParentService()->calculateConcrete($shipment);

        $rate = $this->chooseRate($res);

        $result = new CalculationResult();
        $result->setDeliveryPrice(roundEx(
            $rate['deliveryPrice'],
            SALE_VALUE_PRECISION
        ));

        $extraServicesPrice = 0;
        foreach ($rate['deliveryPriceServices'] as $service) {
            $extraServicesPrice += $service['price'];
        }
        $result->setExtraServicesPrice(roundEx(
            $extraServicesPrice,
            SALE_VALUE_PRECISION
        ));

        $result->setDescription($rate['description']);
        $result->setPeriodDescription(join(' - ', array_filter([$rate['deliveryPeriodMin'], $rate['deliveryPeriodMax']])));
        $result->setData([
            'supportServicePoints' => true,
            'servicePoint' => null,
        ]);

        $order = $shipment->getCollection()->getOrder(); // заказ
        $props = $order->getPropertyCollection();
        $locationCode = $props->getDeliveryLocation()->getValue(); // местоположение

        if(empty($this->getParentService()->locationFlag)) {
            $result->addError(new Error("Данный сервис недоступен для выбранного местоположения"));
        }

        return $result;
    }

    public function isCompatible(Shipment $shipment)
    {
        $res = $this->getParentService()->calculateConcrete($shipment);

        if(empty($this->getParentService()->locationFlag)) {
            return true;
        }

        $rate = $this->chooseRate($res);
        if (empty($rate)) {
            return false;
        }

        return true;
    }
}