<?php
/**
 * Created by PhpStorm.
 * User: tasselchof
 * Date: 13.09.15
 * Time: 16:42
 */
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

define('ADMIN_MODULE_NAME', 'iqcreative.orderadmin');
require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php';

echo Loc::getMessage("IQCREATIVE_ORDERADMIN_INSTALLED");

require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php';