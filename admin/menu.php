<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
IncludeModuleLangFile(__FILE__);
?><?php
/**
 * Created by PhpStorm.
 * User: tasselchof
 * Date: 13.09.15
 * Time: 16:43
 */

defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();
use Bitrix\Main\Localization\Loc;
//Loc::loadMessages(__FILE__);

$aMenu = array(
    array(
        'parent_menu' => 'global_menu_services',
        'sort' => 0,
        'text' => "Orderadmin",
        'title' => "Orderadmin",
        'url' => 'orderadmin_integration_index.php',
        'items_id' => 'menu_references',
        'items' => array(
            /*
            array(
                'text' => "Трпр",
                'url' => 'd7dull_index.php?param1=paramval&lang='.LANGUAGE_ID,
                'more_url' => array('d7dull_index.php?param1=paramval&lang='.LANGUAGE_ID),
                'title' => "Уруруру",
            ),
            */
        ),
    ),
);
return $aMenu;