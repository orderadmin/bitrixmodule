<?php
/**
 * Created by PhpStorm.
 * User: tasselchof
 * Date: 22.09.15
 * Time: 17:04
 */

namespace Bitrix\Orderadmin;

use Bitrix\Main\Localization\Loc;

//require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

class Orderadmin
{
    static $MODULE_ID = 'iqcreative.orderadmin';

    public static function updateItemsAgent()
    {
        Api::updateItemsStats();
        return '\Bitrix\Orderadmin\Orderadmin::updateItemsAgent();';
    }

    public static function createOrdersAgent()
    {
        Api::createOrders();
        return '\Bitrix\Orderadmin\Orderadmin::createOrdersAgent();';
    }
}