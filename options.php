<?php
/**
 * Created by PhpStorm.
 * User: tasselchof
 * Date: 13.09.15
 * Time: 15:19
 */

defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();
defined('ADMIN_MODULE_NAME') or define('ADMIN_MODULE_NAME', 'iqcreative.orderadmin');

if (!CModule::IncludeModule('iblock') || !CModule::IncludeModule('catalog') || !CModule::IncludeModule('sale') || !CModule::IncludeModule('search') || !CModule::IncludeModule('iqcreative.orderadmin')) {
    CHTTP::SetStatus("500 Internal Server Error");
    die('{"error":"Module \"sale\" not installed"}');
}

use Bitrix\Main\Application;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;

//use Bitrix\Main\Text\String;

Loc::loadMessages(__FILE__);

if (!$USER->isAdmin()) {
    $APPLICATION->authForm('Nope');
}

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/' . ADMIN_MODULE_NAME . '/include.php');

$showRightsTab = true;
$arSel = array('REFERENCE_ID' => array(1, 3, 5, 7), 'REFERENCE' => array(Loc::getMessage("IQCREATIVE_ORDERADMIN_ZNACENIE"), Loc::getMessage("IQCREATIVE_ORDERADMIN_ZNACENIE1"), Loc::getMessage("IQCREATIVE_ORDERADMIN_ZNACENIE2"), Loc::getMessage("IQCREATIVE_ORDERADMIN_ZNACENIE3")));

$arTabs = array(
    array(
        'DIV' => 'edit1',
        'TAB' => Loc::getMessage("IQCREATIVE_ORDERADMIN_NASTROYKI"),
        'ICON' => '',
        'TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_NASTROYKI")
    ),
    array(
        'DIV' => 'edit2',
        'TAB' => 'Настройки торгового каталога',
        'ICON' => '',
        'TITLE' => 'Настройки торгового каталога'
    ),
    array(
        'DIV' => 'edit3',
        'TAB' => Loc::getMessage("IQCREATIVE_ORDERADMIN_DELIVERY_SERVICES"),
        'ICON' => '',
        'TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_DELIVERY_SERVICES")
    ),
    array(
        'DIV' => 'edit4',
        'TAB' => Loc::getMessage("IQCREATIVE_ORDERADMIN_PAYMENT_SERVICES"),
        'ICON' => '',
        'TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_PAYMENT_SERVICES")
    ),
);

$arGroups = array(
    'MAIN' => array('TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_DOSTUP_K"), 'TAB' => 0),
    'WAREHOUSE' => array('TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_WAREHOUSE"), 'TAB' => 0),
    'ORDERS' => array('TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_ZAKAZY"), 'TAB' => 0),
    'CATALOG' => array('TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_VYBOR_TORGOVOGO_KATA"), 'TAB' => 1),
    'OFFERS' => array('TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_OFFERS"), 'TAB' => 1),
    'ORDER_FIELDS' => array('TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_ZAKAZY"), 'TAB' => 1),
    'DELIVERY_SERVICES' => array('TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_DELIVERY_SERVICES"), 'TAB' => 2),
    'PAYMENT_SERVICES' => array('TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_PAYMENT_SERVICES"), 'TAB' => 3),
);

CModule::IncludeModule("catalog");

$rsIblock = CCatalog::GetList(array(), array(
    'ACTIVE' => 'Y',
));

$arIblockSel = array(/*
    'REFERENCE_ID' => array(0),
    'REFERENCE' => array(Loc::getMessage("IQCREATIVE_ORDERADMIN_VYBRATQ")),
    */
);

while ($arIblock = $rsIblock->Fetch()) {
    $arIblockSel['REFERENCE_ID'][] = $arIblock['ID'];
    $arIblockSel['REFERENCE'][] = $arIblock['NAME'];
}

$countriesSel = $arDSRates = $arDSSenders = $arProductsShops = $arOrderStatusSel = $arOrderPropsSel = array(
    'REFERENCE_ID' => array(0),
    'REFERENCE' => array(Loc::getMessage("IQCREATIVE_ORDERADMIN_VYBRATQ")),
);

$zip = 0;

$rsOrderProps = CSaleOrderProps::GetList(array(), array(), array('ID', 'NAME'));
while ($arOrderProp = $rsOrderProps->Fetch()) {
    $arOrderPropsSel['REFERENCE_ID'][] = $arOrderProp['ID'];
    $arOrderPropsSel['REFERENCE'][] = '[' . $arOrderProp['ID'] . '] ' . $arOrderProp['NAME'];

    if ($arOrderProp['IS_ZIP'] == 'Y') {
        $zip = $arOrderProp['ID'];
    }
}

$rsOrderStatuses = CSaleStatus::GetList(array(), array(
    'LID' => LANGUAGE_ID,
));
while ($arOrderStatus = $rsOrderStatuses->Fetch()) {
    $arOrderStatusSel['REFERENCE_ID'][] = $arOrderStatus['ID'];
    $arOrderStatusSel['REFERENCE'][] = $arOrderStatus['NAME'];
}

$arOrderDeliverySerivesSel = array();

$rsOrderDeliveryServices = CSaleDelivery::GetList();
while ($arOrderDeliveryService = $rsOrderDeliveryServices->Fetch()) {
    $arOrderDeliverySerivesSel['REFERENCE_ID'][] = $arOrderDeliveryService['ID'];
    $arOrderDeliverySerivesSel['REFERENCE'][] = $arOrderDeliveryService['NAME'];
}

$arDeliveryServicesSel = array();

$obCache = new CPHPCache();
$cache_id = "orderadmin|0.0.3|delivery_services|" . date('d-m-Y');

if ($obCache->InitCache(60 * 60 * 24, $cache_id, "/")) {
    $vars = $obCache->GetVars();

    $deliveryServices = $vars['DELIVERY_SERVICES'];
} else {
    $publicKey = COption::GetOptionString(ADMIN_MODULE_NAME, 'ORDERADMIN_PUBLIC_KEY');
    $secret = COption::GetOptionString(ADMIN_MODULE_NAME, 'ORDERADMIN_SECRET');

    $orderadmin = new \Bitrix\Orderadmin\Api($publicKey, $secret);

    $res = $orderadmin->request('GET', '/delivery-services')->getResult();
    $deliveryServices = json_decode($res, true);

    $obCache->StartDataCache();
    $obCache->EndDataCache(array(
        "DELIVERY_SERVICES" => $deliveryServices
    ));
}

$publicKey = COption::GetOptionString(ADMIN_MODULE_NAME, 'ORDERADMIN_PUBLIC_KEY');
$secret = COption::GetOptionString(ADMIN_MODULE_NAME, 'ORDERADMIN_SECRET');

$orderadmin = new \Bitrix\Orderadmin\Api($publicKey, $secret);

//$loc = \Bitrix\Sale\Location\LocationTable::getByCode(
//    '0000073738', array(
//        'filter' => array('=NAME.LANGUAGE_ID' => LANGUAGE_ID),
//        'select' => array('*', 'NAME_RU' => 'NAME.NAME')
//    )
//)->Fetch();
//
//
//$res = $orderadmin->request(
//    'GET', '/api/locations/localities', array(
//        'criteria' => array(
//            'name'    => $loc['NAME_RU'],
//        ),
//    )
//)->getResult(true);

if (!empty($publicKey) && !empty($secret)) {
    $res = $orderadmin->request('GET', '/api/locations/countries', array('per_page' => 250))->getResult(true);

    if ($res) {
        foreach ($res['_embedded']['countries'] as $country) {
            $countriesSel['REFERENCE_ID'][] = $country['id'];
            $countriesSel['REFERENCE'][] = $country['name'];
        }
    }

    $countryId = Option::get(ADMIN_MODULE_NAME, 'ORDERADMIN_BASE_COUNTRY');

    $criteria = array();
    if (!empty($countryId)) {
        $criteria = array(
            'criteria' => array(
                'country' => [$countryId, ''],
            ),
        );
    }

    $res = $orderadmin->request('GET', '/api/delivery-services', $criteria)->getResult(true);

    $deliveryServicesFilter = [];
    if ($res) {
        foreach ($res['_embedded']['delivery_services'] as $key => $deliveryService) {
            $arDeliveryServicesSel['REFERENCE_ID'][] = $deliveryService['id'];
            $arDeliveryServicesSel['REFERENCE'][] = $deliveryService['name'];

            $deliveryServicesFilter[] = $deliveryService['id'];
        }
    }

    $criteria = array();
    if (!empty($deliveryServicesFilter)) {
        $criteria = array(
            'criteria' => array(
                'state' => 'active',
                'deliveryService' => $deliveryServicesFilter,
            ),
        );
    }

    $res = $orderadmin->request('GET', '/api/delivery-services/rates', $criteria)->getResult(true);

    if ($res) {
        foreach ($res['_embedded']['rates'] as $key => $rate) {
            $arDSRates['REFERENCE_ID'][] = $rate['id'];
            $arDSRates['REFERENCE'][] = sprintf('[%s] %s', $rate['_embedded']['deliveryService']['name'], $rate['name']);
        }
    }

    $res = $orderadmin->request('GET', '/api/delivery-services/senders')->getResult(true);

    if ($res) {
        foreach ($res['_embedded']['senders'] as $sender) {
            $arDSSenders['REFERENCE_ID'][] = $sender['id'];
            $arDSSenders['REFERENCE'][] = sprintf('[%s] %s', $sender['id'], $sender['name']);
        }
    }

    $res = $orderadmin->request('GET', '/api/products/shops')->getResult(true);

    if ($res) {
        foreach ($res['_embedded']['shops'] as $shop) {
            $arProductsShops['REFERENCE_ID'][] = $shop['id'];
            $arProductsShops['REFERENCE'][] = sprintf('[%s] %s', $shop['id'], $shop['name']);;
        }
    }

    $res = $orderadmin->request('GET', '/api/integrations/sources', array(
        'criteria' => array(
            'handler' => 'bitrix',
            'name' => COption::GetOptionString('main', 'server_name'),
        ),
    ))->getResult(true);

    if ($res['total_items'] == 0) {
        $res = $orderadmin->setRequest(array(
            'handler' => 'bitrix',
            'name' => COption::GetOptionString('main', 'server_name'),
            'settings' => array(
                'url' => sprintf('https://%s', COption::GetOptionString('main', 'server_name')),
            ),
        ))->request('POST', '/api/integrations/sources')->getResult(true);
    }
}

$arPaymentServicesSel = array(/*
    'REFERENCE_ID' => array(0),
    'REFERENCE' => array(Loc::getMessage("IQCREATIVE_ORDERADMIN_VYBRATQ")),
    */
);

$arFilter = array(
    "ACTIVE" => "Y",
);

$dbPaySystem = CSalePaySystem::GetList(array("SORT" => "ASC", "PSA_NAME" => "ASC"), $arFilter);
while ($arPaySystem = $dbPaySystem->Fetch()) {
    $arPaymentServicesSel['REFERENCE_ID'][] = $arPaySystem['ID'];
    $arPaymentServicesSel['REFERENCE'][] = $arPaySystem['NAME'];
}

$catalogId = COption::GetOptionString(ADMIN_MODULE_NAME, 'ORDERADMIN_CATALOG');

$arPropertiesSel = array(
    'REFERENCE_ID' => array('PREVIEW_PICTURE', 'DETAIL_PICTURE'),
    'REFERENCE' => array('Картинка для анонса', 'Детальная картинка'),
);

if (!empty($catalogId)) {
    $rsProperties = CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"), Array("ACTIVE" => "Y", "IBLOCK_ID" => $catalogId));
    while ($arProperty = $rsProperties->Fetch()) {
        $arPropertiesSel['REFERENCE_ID'][] = $arProperty['ID'];
        $arPropertiesSel['REFERENCE'][] = $arProperty['NAME'];
    }

    $catalogIBlockProps = CCatalogSKU::GetInfoByProductIBlock($catalogId);

    if (is_numeric($catalogIBlockProps['IBLOCK_ID'])) {
        $arOfferPropertiesSel = array(
            'REFERENCE_ID' => array('PREVIEW_PICTURE', 'DETAIL_PICTURE'),
            'REFERENCE' => array('Картинка для анонса', 'Детальная картинка'),
        );

        if (!empty($catalogId)) {
            $rsProperties = CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"), Array("ACTIVE" => "Y", "IBLOCK_ID" => $catalogIBlockProps['IBLOCK_ID']));
            while ($arProperty = $rsProperties->Fetch()) {
                $arOfferPropertiesSel['REFERENCE_ID'][] = $arProperty['ID'];
                $arOfferPropertiesSel['REFERENCE'][] = $arProperty['NAME'];
            }
        }
    }
}

$arOptions = array(
    'ORDERADMIN_SERVER' => array(
        'GROUP' => 'MAIN',
        'TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_SERVER"),
        'TYPE' => 'STRING',
        'DEFAULT' => 'panel.orderadmin.ru',
        'SORT' => '10',
        'NOTES' => ''
    ),
    'ORDERADMIN_API_URL' => array(
        'GROUP' => 'MAIN',
        'TITLE' => 'API URL',
        'TYPE' => 'STRING',
        'DEFAULT' => 'http://panel.orderadmin.ru/api/rest/latest',
        'SORT' => '10',
        'NOTES' => ''
    ),
    'ORDERADMIN_PUBLIC_KEY' => array(
        'GROUP' => 'MAIN',
        'TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_PUBLICNYY_KLUC"),
        'TYPE' => 'STRING',
        'DEFAULT' => '',
        'SORT' => '15',
        'NOTES' => ''
    ),
    'ORDERADMIN_SECRET' => array(
        'GROUP' => 'MAIN',
        'TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_SEKRETNAA_STROKA"),
        'TYPE' => 'STRING',
        'DEFAULT' => '',
        'SORT' => '20',
        'NOTES' => ''
    ),
    'ORDERADMIN_API_STORAGE_URL' => array(
        'GROUP' => 'MAIN',
        'TITLE' => 'Storage API URL',
        'TYPE' => 'STRING',
        'DEFAULT' => '',
        'SORT' => '25',
        'NOTES' => ''
    ),
    'ORDERADMIN_API_STATUS_URL' => array(
        'GROUP' => 'MAIN',
        'TITLE' => 'Status API URL',
        'TYPE' => 'STRING',
        'DEFAULT' => '',
        'SORT' => '25',
        'NOTES' => ''
    ),
    'ORDERADMIN_AGENTS_QUANTITY' => array(
        'GROUP' => 'WAREHOUSE',
        'TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_AGENTS_QUANTITY"),
        'TYPE' => 'CHECKBOX',
        'VALUE' => '1',
        'SORT' => '60'
    ),
    'ORDERADMIN_BASE_COUNTRY' => array(
        'GROUP' => 'WAREHOUSE',
        'TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_BAZOVAA_STRANA"),
        'TYPE' => 'SELECT',
        'VALUES' => $countriesSel,
        'DEFAULT' => '',
        'SORT' => '40',
        'NOTES' => ''
    ),
    'ORDERADMIN_SHOP' => array(
        'GROUP' => 'WAREHOUSE',
        'TITLE' => 'Магазин',
        'TYPE' => 'SELECT',
        'VALUES' => $arProductsShops,
        'SORT' => '45',
        'NOTES' => ''
    ),
    'ORDERADMIN_SENDER' => array(
        'GROUP' => 'WAREHOUSE',
        'TITLE' => 'Отправитель',
        'TYPE' => 'SELECT',
        'VALUES' => $arDSSenders,
        'DEFAULT' => '',
        'SORT' => '50',
        'NOTES' => ''
    ),
    'ORDERADMIN_WAREHOUSE' => array(
        'GROUP' => 'WAREHOUSE',
        'TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_SKLAD_OTGRUZKI_ZAKAZ"),
        'TYPE' => 'STRING',
        'DEFAULT' => '',
        'SORT' => '50',
        'NOTES' => ''
    ),
    'ORDERADMIN_BASE_WEIGHT' => array(
        'GROUP' => 'WAREHOUSE',
        'TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_BAZOVYY_VES_GRAMM"),
        'TYPE' => 'STRING',
        'DEFAULT' => '',
        'SORT' => '55',
        'NOTES' => Loc::getMessage("IQCREATIVE_ORDERADMIN_UKAJITE_EGO_DLA_RASS")
    ),
    'ORDERADMIN_AGENTS_WEIGHT' => array(
        'GROUP' => 'WAREHOUSE',
        'TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_AGENTS_WEIGHT"),
        'TYPE' => 'CHECKBOX',
        'VALUE' => '1',
        'SORT' => '70'
    ),
    'ORDERADMIN_AGENTS_VOLUME' => array(
        'GROUP' => 'WAREHOUSE',
        'TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_AGENTS_VOLUME"),
        'TYPE' => 'CHECKBOX',
        'VALUE' => '1',
        'SORT' => '80'
    ),
    'ORDERADMIN_ORDER_DAYS_LIMIT' => array(
        'GROUP' => 'ORDERS',
        'TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_ORDER_DAYS_LIMIT"),
        'TYPE' => 'STRING',
        'VALUE' => '',
        'DEFAULT' => '432000',
        'SORT' => '105'
    ),
    'ORDERADMIN_ORDER_STATUSES' => array(
        'GROUP' => 'ORDERS',
        'TITLE' => 'Фильтровать заказы по статусу',
        'TYPE' => 'MCHECKBOX',
        'VALUES' => $arOrderStatusSel,
        'SORT' => '110'
    ),
    'ORDERADMIN_ORDER_CHECK_DELIVERY' => array(
        'GROUP' => 'ORDERS',
        'TITLE' => 'Проверять разрешение доставки',
        'TYPE' => 'CHECKBOX',
        'DEFAULT' => '',
        'SORT' => '125'
    ),
    'ORDERADMIN_CATALOG' => array(
        'GROUP' => 'CATALOG',
        'TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_TORGOVYY_KATALOG"),
        'TYPE' => 'SELECT',
        'VALUES' => $arIblockSel,
        'SORT' => '60'
    ),
    'ORDERADMIN_CATALOG_CHECK_PRODUCT_AVAILABILITY' => array(
        'GROUP' => 'CATALOG',
        'TITLE' => 'Проверять наличие товара',
        'TYPE' => 'CHECKBOX',
        'DEFAULT' => 'Y',
        'SORT' => '75'
    ),
    'ORDERADMIN_CATALOG_ARTICLE' => array(
        'GROUP' => 'CATALOG',
        'TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_SVOYSTVO_SODERJASEE"),
        'TYPE' => 'SELECT',
        'VALUES' => $arPropertiesSel,
        'ALLOW_EMPTY' => 'Y',
        'DEFAULT' => '',
        'SORT' => '80',
    ),
    'ORDERADMIN_CATALOG_FIELDS_IMAGE' => array(
        'GROUP' => 'CATALOG',
        'TITLE' => 'Поле с картинкой',
        'TYPE' => 'SELECT',
        'VALUES' => $arPropertiesSel,
        'ALLOW_EMPTY' => 'Y',
        'DEFAULT' => 'PREVIEW_PICTURE',
        'SORT' => '85'
    ),
    'ORDERADMIN_CATALOG_FIELDS_MODEL' => array(
        'GROUP' => 'CATALOG',
        'TITLE' => 'Поле, содержащее модель',
        'TYPE' => 'SELECT',
        'VALUES' => $arPropertiesSel,
        'ALLOW_EMPTY' => 'Y',
        'DEFAULT' => '',
        'SORT' => '87'
    ),
    'ORDERADMIN_CATALOG_ADDITIONAL_FIELDS' => array(
        'GROUP' => 'CATALOG',
        'TITLE' => 'Выгружать дополнительные свойства',
        'TYPE' => 'MSELECT',
        'VALUES' => $arPropertiesSel,
        'ALLOW_EMPTY' => 'N',
        'SORT' => '89'
    ),
    'ORDERADMIN_ORDER_FIELDS_PHONE' => array(
        'GROUP' => 'ORDER_FIELDS',
        'TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_PHONE"),
        'TYPE' => 'SELECT',
        'VALUES' => $arOrderPropsSel,
        'SORT' => '87'
    ),
    'ORDERADMIN_ORDER_FIELDS_POSTCODE' => array(
        'GROUP' => 'ORDER_FIELDS',
        'TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_POCTOVYY_INDEKS"),
        'TYPE' => 'SELECT',
        'VALUES' => $arOrderPropsSel,
        'DEFAULT' => $zip,
        'SORT' => '90'
    ),
    'ORDERADMIN_ORDER_FIELDS_ADDRESS' => array(
        'GROUP' => 'ORDER_FIELDS',
        'TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_ORDER_FIELDS_ADDRESS"),
        'TYPE' => 'SELECT',
        'VALUES' => $arOrderPropsSel,
        'SORT' => '100'
    ),
    'ORDERADMIN_ORDER_FIELDS_STREET' => array(
        'GROUP' => 'ORDER_FIELDS',
        'TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_ULICA"),
        'TYPE' => 'SELECT',
        'VALUES' => $arOrderPropsSel,
        'SORT' => '110'
    ),
    'ORDERADMIN_ORDER_FIELDS_HOME' => array(
        'GROUP' => 'ORDER_FIELDS',
        'TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_DOM"),
        'TYPE' => 'SELECT',
        'VALUES' => $arOrderPropsSel,
        'SORT' => '120'
    ),
    'ORDERADMIN_ORDER_FIELDS_BLOCK' => array(
        'GROUP' => 'ORDER_FIELDS',
        'TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_BLOK"),
        'TYPE' => 'SELECT',
        'VALUES' => $arOrderPropsSel,
        'SORT' => '130'
    ),
    'ORDERADMIN_ORDER_FIELDS_APARTMENT' => array(
        'GROUP' => 'ORDER_FIELDS',
        'TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_KVARTIRA"),
        'TYPE' => 'SELECT',
        'VALUES' => $arOrderPropsSel,
        'SORT' => '140'
    ),
    'ORDERADMIN_DELIVERY_SERVICES' => array(
        'GROUP' => 'DELIVERY_SERVICES',
        'TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_DELIVERY_SERVICES"),
        'TYPE' => 'MCHECKBOX',
        'VALUES' => $arDeliveryServicesSel,
        'SORT' => '10'
    ),
    'ORDERADMIN_DELIVERY_SERVICES_DEFAULT_RATE' => array(
        'GROUP' => 'DELIVERY_SERVICES',
        'TITLE' => 'Тариф по умолчанию',
        'TYPE' => 'SELECT',
        'VALUES' => $arDSRates,
        'SORT' => '20',
        'NOTES' => ''
    ),
    'ORDERADMIN_PREPAYMENT_SERVICES' => array(
        'GROUP' => 'PAYMENT_SERVICES',
        'TITLE' => Loc::getMessage("IQCREATIVE_ORDERADMIN_PREPAYMENT_SERVICES"),
        'TYPE' => 'MCHECKBOX',
        'VALUES' => $arPaymentServicesSel,
        'SORT' => '20'
    ),
);

if (is_numeric($catalogIBlockProps['IBLOCK_ID'])) {
    $arOptions = array_merge($arOptions, array(
        'ORDERADMIN_OFFER_FIELDS_MODEL' => array(
            'GROUP' => 'OFFERS',
            'TITLE' => 'Поле, содержащее модель',
            'TYPE' => 'SELECT',
            'VALUES' => $arOfferPropertiesSel,
            'ALLOW_EMPTY' => 'Y',
            'DEFAULT' => '',
            'SORT' => '87'
        ),
        'ORDERADMIN_OFFER_FIELDS_ARTICLE' => array(
            'GROUP' => 'OFFERS',
            'TITLE' => 'Поле, содержащее артикул',
            'TYPE' => 'SELECT',
            'VALUES' => $arOfferPropertiesSel,
            'ALLOW_EMPTY' => 'Y',
            'DEFAULT' => '',
            'SORT' => '87'
        ),
        'ORDERADMIN_OFFER_FIELDS_IMAGE' => array(
            'GROUP' => 'OFFERS',
            'TITLE' => 'Поле с картинкой',
            'TYPE' => 'SELECT',
            'VALUES' => $arOfferPropertiesSel,
            'ALLOW_EMPTY' => 'Y',
            'DEFAULT' => 'PREVIEW_PICTURE',
            'SORT' => '85'
        ),
        'ORDERADMIN_OFFER_ADDITIONAL_FIELDS' => array(
            'GROUP' => 'OFFERS',
            'TITLE' => 'Выгружать дополнительные свойства',
            'TYPE' => 'MSELECT',
            'VALUES' => $arOfferPropertiesSel,
            'ALLOW_EMPTY' => 'N',
            'SORT' => '89'
        ),
    ));
}

$rsSites = CSite::GetList($by = "sort", $order = "desc", Array("ACTIVE" => "Y"));
if ($rsSites->SelectedRowsCount() > 1) {
    $isDefault = false;
    $site = false;
    while ($arSite = $rsSites->Fetch()) {
        if ($_SERVER['HTTP_HOST'] == $arSite['SERVER_NAME']) {
            break;
        }
    }

    if ($arSite['DEF'] != 'Y') {
        $prefix = strtoupper($arSite['LID']);

        foreach ($arOptions as $key => $value) {
            $arOptions[join('_', array($prefix, $key))] = $value;

            unset($arOptions[$key]);
        }
    }
}

$opt = new CModuleOptions(ADMIN_MODULE_NAME, $arTabs, $arGroups, $arOptions, $showRightsTab);
$opt->ShowHTML();

if (!empty($_REQUEST['q'])) {
    $loc = \Bitrix\Sale\Location\LocationTable::getByCode(
        '0000103664', array(
            'filter' => array('=NAME.LANGUAGE_ID' => LANGUAGE_ID),
            'select' => array('*', 'NAME_RU' => 'NAME.NAME')
        )
    )->Fetch();

    $res = $orderadmin->request(
        'GET', '/api/locations/localities', array(
        'filter' => array(
            array(
                'type'  => 'eq',
                'field' => 'name',
                'value' => $loc['NAME_RU'],
            ),
            array(
                'type'   => 'in',
                'field'  => 'type',
                'values' => array(
                    'Город',
                ),
            ),
        ),
    ), true
    )->getResult(true);
}

if (!empty($_POST)) {
    $agentIterator = CAgent::GetList(
        array(),
        array('MODULE_ID' => MODULE_NAME, '=NAME' => '\Bitrix\Orderadmin\Orderadmin::updateItemsAgent();')
    );

    if ($orderAdminAgent = $agentIterator->Fetch()) {
        $active = ($action == 'activate' ? 'Y' : 'N');

        CAgent::Update($orderAdminAgent['ID'], array('ACTIVE' => $_REQUEST['ORDERADMIN_AGENTS_QUANTITY']));
    } else {
        $checkDate = DateTime::createFromTimestamp(strtotime('+1hour'));;

        $r = new CAgent;
        $r->AddAgent('\Bitrix\Orderadmin\Orderadmin::updateItemsAgent();', ADMIN_MODULE_NAME, 'Y', 3600, '', 'Y', $checkDate->toString(), 100, false, true);
    }
}

if (!empty($_REQUEST['test'])) {
    \Bitrix\Orderadmin\Api::createDeliveryRequests();
}