<?php
/**
 * Created by PhpStorm.
 * User: tasselchof
 * Date: 13.09.15
 * Time: 16:58
 */

define('MODULE_NAME', 'iqcreative.orderadmin');
define('NO_AGENT_CHECK', true);
define('NO_AGENT_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('DisableEventsCheck', true);

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

if (!CModule::IncludeModule('iqcreative.orderadmin') || !CModule::IncludeModule('sale')) {
    CHTTP::SetStatus("500 Internal Server Error");
    die('{"error":"Module \"sale\" not installed"}');
}

$publicKey = COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_PUBLIC_KEY');
$secret = COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_SECRET');

//$key = hash_hmac('sha1', date('Y-m-d'), $secret);

$prefix = '';

$rsSites = CSite::GetList($by="sort", $order="desc", Array("ACTIVE" => "Y"));
if($rsSites->SelectedRowsCount() > 1) {
    $rsSite = CSite::GetById(SITE_ID);
    $arSite = $rsSite->Fetch();

    if($arSite['DEF'] != 'Y') {
        $prefix = strtoupper($arSite['LID']) . '_';
    }
}

$orderadmin = new \Bitrix\Orderadmin\Api($publicKey, $secret);

switch ($_REQUEST['query']) {
    case 'calculator':
        header('Content-type: application/json');

        $request = json_decode(file_get_contents('php://input'), true);

        if (!empty($_REQUEST['basket'])) {
            $arBasketItems = array();

            $dbBasketItems = CSaleBasket::GetList(
                array(
                    "NAME" => "ASC",
                    "ID" => "ASC"
                ),
                array(
                    "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                    "LID" => SITE_ID,
                    "ORDER_ID" => "NULL"
                ),
                false,
                false,
                array("ID",
                    "MODULE",
                    "PRODUCT_ID",
                    "QUANTITY",
                    "DELAY",
                    "CAN_BUY",
                    "PRICE",
                    "WEIGHT")
            );

            $weight = 0;
            $payment = 0;
            $estimatedCost = 0;
            while ($arItem = $dbBasketItems->Fetch()) {
                $arBasketItems[] = $arItem;

                $weight += $arItem['WEIGHT'];
                $payment += $arItem['PRICE'];
                $estimatedCost += $arItem['PRICE'];
            }

            if($weight > 0) {
                $request['weight'] = $weight;
            }

            if($estimatedCost > 0) {
                $request['estimatedCost'] = $weight;
            }

            $prepaymentServices = array_filter(unserialize(COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_PREPAYMENT_SERVICES')));
            if(isset($_REQUEST['paysystem']) && in_array($_REQUEST['paysystem'], $prepaymentServices) && $payment > 0) {
                $request['payment'] = $payment;
            }

            $deliveryServices = array_filter(unserialize(COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_PREPAYMENT_DELIVERY_SERVICES')));

            if(!empty($deliveryServices)) {
                $request['delivery-services'] = array();

                foreach($deliveryServices as $deliveryServiceId) {
                    $request['delivery-services'][] = array(
                        'id' => $deliveryServiceId,
                    );
                }
            }
        }

        $res = $orderadmin->setRequest($request)->request('POST', '/delivery-services/calculator')->getResult();

        if ($res && $_REQUEST['basket']) {
            $orderadmin->saveCalculationResult();
        }
        break;

    case 'detect':
        $res = $orderadmin->setRequest(array())->request('GET', '/locations/detect?ip=109.252.62.235')->getResult();
        break;

    case 'localities':
        $res = $orderadmin->setRequest(array())->request('GET', '/locations/' . $_REQUEST['country'] . '?queryString=' . $_REQUEST['queryString'] . '%')->getResult();
        break;
}

if (!$res) {
    echo $orderadmin->getError();
} else {
    echo $res;
}

require($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/include/epilog_after.php");
?>