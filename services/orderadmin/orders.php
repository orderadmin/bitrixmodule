<?php
/**
 * Created by PhpStorm.
 * User: tasselchof
 * Date: 13.09.15
 * Time: 16:58
 */

if (!defined('MODULE_NAME')) die();

$warehouse = COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_WAREHOUSE');
$orderDaysLimit = COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_ORDER_DAYS_LIMIT');
$prepaymentServices = array_filter(unserialize(COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_PREPAYMENT_SERVICES')));

$reserveLimit = COption::GetOptionString('sale', 'product_reserve_clear_period');

$orderCheckDelivery = COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_ORDER_CHECK_DELIVERY');
$orderStatuses = COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_ORDER_STATUSES');
$orderDeliveryServices = COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_ORDER_DELIVERY_SERVICES');

if (!is_numeric($orderDaysLimit)) {
    $orderDaysLimit = 5;
}

if (!isset($_REQUEST['format'])) {
    $format = 'xml';
} else {
    $format = $_REQUEST['format'];
}

$result = array();

$arFilter = Array(
    ">=DATE_MODIFIED" => date($DB->DateFormatToPHP(CSite::GetDateFormat("SHORT")), mktime() - $orderDaysLimit)
);

if(isset($orderCheckDelivery) && $orderCheckDelivery == 'Y') {
    $arFilter['ALLOW_DELIVERY'] = 'Y';
}

if(isset($orderStatuses) && !empty($orderStatuses)) {
    $arFilter['@STATUS_ID'] = unserialize($orderStatuses);
}

if(isset($_REQUEST['page']) && isset($_REQUEST['elements'])) {
    $arNavStartParams = array(
        'iNumPage' => $_REQUEST['page'],
        'nPageSize' => $_REQUEST['elements'],
    );
} else {
    $arNavStartParams = false;
}

$rsOrders = CSaleOrder::GetList(array("DATE_INSERT" => "DESC"), $arFilter, false, $arNavStartParams, array('*'));
while ($arOrder = $rsOrders->Fetch()) {
    $props = array();
    $propLocation = null;
    $rsOrderProps = CSaleOrderPropsValue::GetOrderProps($arOrder['ID']);

    while ($arValue = $rsOrderProps->Fetch()) {
        $props[$arValue['ORDER_PROPS_ID']] = $arValue;

        if($arValue['IS_LOCATION'] == 'Y') {
            $propLocation = $arValue;
        }
    }

    $order = array(
        'id' => $arOrder['ID'],
        'extId' => $arOrder['ACCOUNT_NUMBER'],
        'warehouse' => $warehouse,
        'date' => $arOrder['DATE_INSERT'],
        'canceled' => $arOrder['CANCELED'] == 'Y' ? true : false,
        'comments' => $arOrder['USER_DESCRIPTION'],
        'additionalInfo' => $arOrder['COMMENTS'],
    );

    // Payment
    $prepayment = false;
    if(!empty($prepaymentServices) && array_search($arOrder['PAY_SYSTEM_ID'], $prepaymentServices) !== false) {
        $prepayment = true;
    }

    $order['payment'] = array(
        'prepayed' => $prepayment,
        'payed' => $arOrder['PAYED'] == 'Y' ? true : false,
        'payment' => $arOrder['PRICE'] - $arOrder['SUM_PAID'],
        'date' => !empty($arOrder['DATE_PAYED']) ? $arOrder['DATE_PAYED'] : null,
    );

    // Mark
    if($arOrder['MARKED'] == 'Y') {
        $order['mark'] = array(
            'marked' => true,
            'reason' => $arOrder['REASON_MARKED'],
        );
    }

    // Deduction
    $order['deduction'] = array(
        'allowed' => $arOrder['DEDUCTED'] == 'Y' ? true : false,
        'reason' => !empty($arOrder['REASON_UNDO_DEDUCTED']) ? $arOrder['REASON_UNDO_DEDUCTED'] : null,
    );

    // Delivery
    $order['delivery'] = array(
        'allowed' => $arOrder['ALLOW_DELIVERY'] == 'Y' ? true : false,
        'delivery' => $arOrder['DELIVERY_ID'],
        'date' => $arOrder['DATE_ALLOW_DELIVERY'],
        'price' => $arOrder['PRICE_DELIVERY'],
        'document' => $arOrder['DELIVERY_DOC_NUM'],
        'documentDate' => $arOrder['DELIVERY_DOC_DATE'],
    );

    // Reserve
    $order['reserve'] = array(
        'allowed' => $arOrder['RESERVED'] == 'Y' ? true : false,
        'limit' => ($arOrder['RESERVED'] == 'Y' && $reserveLimit > 0) ? $reserveLimit * 24 * 60 * 60 : null,
    );

    // Delivery
    $delivery = explode(':', $arOrder['DELIVERY_ID']);
    if ($delivery[0] == 'orderadmin') {
        $order['delivery'] = array(
            'allowed' => $arOrder['ALLOW_DELIVERY'] == 'Y' ? true : false,
            'delivery_service' => $delivery[0],
            'date' => isset($arOrder['DATE_ALLOW_DELIVERY']) && !empty($arOrder['DATE_ALLOW_DELIVERY']) ? $arOrder['DATE_ALLOW_DELIVERY'] : null,
            'id' => isset($delivery[1]) ? $delivery[1] : null,
        );
    }

    // Recipient
    $order['recipient']['user'] = array(
        'id' => $arOrder['USER_ID'],
        'name' => $arOrder['USER_NAME'],
        'login' => $arOrder['USER_LOGIN'],
        'last_name' => $arOrder['USER_LAST_NAME'],
        'email' => $arOrder['USER_EMAIL'],
    );

    $order['recipient']['phone'] = $props[COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_ORDER_FIELDS_PHONE')]['VALUE'];

    // Address
    if(!is_null($propLocation)) {
        $arLocation = CSaleLocation::GetByID($propLocation['VALUE']);
    } else {
        $arLocation = null;
    }

    $order['recipient']['address'] = array(
        'postcode' => $props[COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_ORDER_FIELDS_POSTCODE')]['VALUE'],
        'country' => COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_BASE_COUNTRY'),
        'city' => array(
            'name' => !is_null($arLocation) ? $arLocation['CITY_NAME_LANG'] : null,
            'extId' => !is_null($propLocation) ? $propLocation['VALUE'] : null,
        ),
        'street' => $props[COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_ORDER_FIELDS_STREET')]['VALUE'],
        'house' => $props[COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_ORDER_FIELDS_HOME')]['VALUE'],
        'block' => $props[COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_ORDER_FIELDS_BLOCK')]['VALUE'],
        'apartment' => $props[COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_ORDER_FIELDS_APARTMENT')]['VALUE'],
        'not_formal' => $props[COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_ORDER_FIELDS_ADDRESS')]['VALUE'],
    );

    // Goods
    $rsBasketItems = CSaleBasket::GetList(
        array(
            "NAME" => "ASC",
            "ID" => "ASC"
        ),
        array(
            "ORDER_ID" => $arOrder['ID']
        ),
        false,
        false,
        array('PRODUCT_ID', 'NAME', 'PRICE', 'QUANTITY', 'DISCOUNT_PRICE')
    );

    // Goods
    $goodsTotal = 0;
    $goods = array();
    while ($arItem = $rsBasketItems->Fetch()) {
        $good = array(
            'id' => $arItem['PRODUCT_ID'],
            'name' => $arItem['NAME'],
            'price' => $arItem['PRICE'] + $arItem['DISCOUNT_PRICE'],
            'discount_price' => $arItem['PRICE'],
            'discount' => $arItem['DISCOUNT_PRICE'],
            'quantity' => $arItem['QUANTITY'],
            'tax' => $arItem['TAX_VALUE'],
            'total' => $arItem['PRICE'] * $arItem['QUANTITY'],
        );

        if($format == 'xml') {
            $goods[]['good'] = $good;
        } else {
            $goods[] = $good;
        }

        $goodsTotal += $good['total'];
    }

    $order['goods'] = $goods;

    // Price
    $order['price'] = array(
        'price' => $goodsTotal,
        'currency' => $arOrder['CURRENCY'],
        'delivery' => $arOrder['PRICE_DELIVERY'],
        'discount' => $arOrder['DISCOUNT_VALUE'] > 0 ? $arOrder['PRICE'] - $arOrder['DISCOUNT_VALUE'] : 0,
        'tax' => $arOrder['TAX_VALUE'],
        'total' => $arOrder['PRICE'],
    );

    if($format == 'xml') {
        $result[]['order'] = $order;
    } else {
        $result[] = $order;
    }
}

switch ($format) {
    case 'xml':
        function arrayToXml($data, &$xmlData)
        {
            foreach ($data as $key => $value) {
                if (is_array($value)) {
                    if (is_numeric($key)) {
                        $key = key($value);
                        $value = $value[$key];
                    }
                    $subnode = $xmlData->addChild($key);
                    arrayToXml($value, $subnode);
                } else {
                    $xmlData->addChild("$key", htmlspecialchars("$value"));
                }
            }
        }

        $xml = new SimpleXMLElement('<orders />');
        arrayToXml($result, $xml);

        header('Content-Type: text/xml; charset=utf-8');
        echo $xml->asXML();
        break;

    case 'json':
        header('Content-Type: application/json');

        echo json_encode($result);
        break;
}

?>