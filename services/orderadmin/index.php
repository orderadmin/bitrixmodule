<?php
/**
 * Created by PhpStorm.
 * User: tasselchof
 * Date: 13.09.15
 * Time: 16:58
 */

define('MODULE_NAME', 'iqcreative.orderadmin');
define('NO_AGENT_CHECK', true);
define('NO_AGENT_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('DisableEventsCheck', true);

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

Bitrix\Main\Loader::includeModule("iblock");
Bitrix\Main\Loader::includeModule("catalog");
Bitrix\Main\Loader::includeModule("sale");
Bitrix\Main\Loader::includeModule("search");
Bitrix\Main\Loader::includeModule("iqcreative.orderadmin");

$prefix = '';

$rsSites = CSite::GetList($by = "sort", $order = "desc", Array("ACTIVE" => "Y"));
if ($rsSites->SelectedRowsCount() > 1) {
    $rsSite = CSite::GetById(SITE_ID);
    $arSite = $rsSite->Fetch();

    if ($arSite['DEF'] != 'Y') {
        $prefix = strtoupper($arSite['LID']) . '_';
    }
}

$catalogId = COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_CATALOG');
$catalogArticleId = COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_CATALOG_ARTICLE');

if (empty($catalogId) || !is_numeric($catalogArticleId)) {
    CHTTP::SetStatus("500 Internal Server Error");
    die('{"error":"Options didn\'t set"}');
}

switch ($_REQUEST['type']) {
    case 'locations':
        require_once(__DIR__ . '/locations.php');
        break;

    case 'products':
        \Bitrix\Orderadmin\Api::checkAuthorization($_REQUEST['authorization']);

        require_once(__DIR__ . '/products.php');
        break;

    case 'orders':
        \Bitrix\Orderadmin\Api::checkAuthorization($_REQUEST['authorization']);

        require_once(__DIR__ . '/orders.php');
        break;

    case 'calculation':
        header('Content-Type: application/json');

        if (!empty($_SESSION['oaCurrentCalculation'])) {
            echo json_encode($_SESSION['oaCurrentCalculation']);
        } else {
            echo '{}';
        }
        break;

    case 'delivery_profiles':
        $dbSubServicesRes = \Bitrix\Sale\Delivery\Services\Table::getList(array(
            "filter" => array(),
        ));

        $rates = array();
        while ($arProfile = $dbSubServicesRes->Fetch()) {
            $rates[] = $arProfile;
        }

        header('Content-Type: application/json');

        echo json_encode($rates);
        break;
}

require($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/include/epilog_after.php");
?>