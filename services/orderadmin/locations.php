<?php
/**
 * Created by PhpStorm.
 * User: tasselchof
 * Date: 13.09.15
 * Time: 16:58
 */

if (!defined('MODULE_NAME')) die();

$xml = new domDocument("1.0", "utf-8");
$xml->preserveWhiteSpace = false;
$xml->formatOutput = true;

$root = $xml->createElement('cities');
$root->setAttribute('date', date('Y-m-d H:i'));
$xml->appendChild($root);

$arOrder = array(
    "SORT" => "ASC",
    "COUNTRY_NAME_LANG" => "ASC",
    "CITY_NAME_LANG" => "ASC",
);

$arFilter = array(
    '!CITY_ID' => false,
    'LID' => LANGUAGE_ID,
);

$rsLocations = \Bitrix\Sale\Location\LocationTable::getList(array(
    'filter' => array('=NAME.LANGUAGE_ID' => LANGUAGE_ID),
    'select' => array('*', 'NAME_RU' => 'NAME.NAME', 'TYPE_CODE' => 'TYPE.CODE')
));
while ($arLocation = $rsLocations->Fetch()) {
    $city = $xml->createElement('location');
    $root->appendChild($city);

    $zip = \Bitrix\Sale\Location\ExternalTable::getList(array(
        'filter' => array(
            '=SERVICE.CODE' => CSaleLocation::ZIP_EXT_SERVICE_CODE,
            '=LOCATION_ID' => $arLocation['ID']
        ),
        'select' => array(
            'ID',
            'ZIP' => 'XML_ID'
        )
    ))->fetch();

    $city->appendChild($xml->createElement('locationId', $arLocation['ID']));
    $city->appendChild($xml->createElement('locationCode', $arLocation['CODE']));
    $city->appendChild($xml->createElement('location', $arLocation['NAME_RU']));
    $city->appendChild($xml->createElement('postcode', $zip['ZIP']));
}

header('Content-Type: text/xml; charset=utf-8');
$xmlString = html_entity_decode($xml->saveHTML(), ENT_QUOTES, "UTF-8");
echo preg_replace('/(?:^|\G)  /um', "\t", $xmlString);

require($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/include/epilog_after.php");
?>