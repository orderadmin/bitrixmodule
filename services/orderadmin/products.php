<?php
/**
 * Created by PhpStorm.
 * User: tasselchof
 * Date: 13.09.15
 * Time: 16:58
 */

if (!defined('MODULE_NAME')) die();

$xml = new domDocument("1.0", "utf-8");
$xml->preserveWhiteSpace = false;
$xml->formatOutput = true;

$root = $xml->createElement('yml_catalog');
$root->setAttribute('date', date('Y-m-d H:i'));
$xml->appendChild($root);

$shop = $xml->createElement('shop');
$root->appendChild($shop);

$shop->appendChild($xml->createElement('name', COption::GetOptionString('main', 'site_name')));
$shop->appendChild($xml->createElement('company', COption::GetOptionString('main', 'site_name')));
$shop->appendChild($xml->createElement('url', COption::GetOptionString('main', 'server_name')));
$shop->appendChild($xml->createElement('platform', 'Orderadmin'));

if(isset($_REQUEST['page']) && isset($_REQUEST['elements'])) {
    $arNavStartParams = array(
        'iNumPage' => $_REQUEST['page'],
        'nPageSize' => $_REQUEST['elements'],
    );
} else {
    $arNavStartParams = false;
}

$productModelFieldId = COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_CATALOG_FIELDS_MODEL');
$productArticleFieldId = COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_CATALOG_ARTICLE');
$productPictureFieldId = COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_CATALOG_FIELDS_IMAGE');

$checkAvailability = COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_CATALOG_CHECK_PRODUCT_AVAILABILITY');

$catalogProducts = array();

if(is_numeric($productPictureFieldId)) {
    $productPictureFieldId = $productPictureFieldId;
}

$productSelectFields = array('ID', 'IBLOCK_ID', 'NAME', 'IBLOCK_SECTION_ID', 'PREVIEW_TEXT');
if (is_numeric($productPictureFieldId)) {
    $productSelectFields[] = 'PROPERTY_' . $productPictureFieldId;
} elseif (is_string($productPictureFieldId)) {
    $productSelectFields[] = $productPictureFieldId;
}

if (!empty($productModelFieldId)) {
    $productSelectFields[] = 'PROPERTY_' . $productModelFieldId;
}

if (!empty($productArticleFieldId)) {
    $productSelectFields[] = 'PROPERTY_' . $productArticleFieldId;
}

$catalogAdditionalFields = unserialize(COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_CATALOG_ADDITIONAL_FIELDS'));
if(!empty($catalogAdditionalFields)) {
    foreach ($catalogAdditionalFields as $fieldId) {
        $productSelectFields[] = 'PROPERTY_' . $fieldId;
    }
}

$arFilter = Array('IBLOCK_ID' => $catalogId);
if(isset($checkAvailability) && $checkAvailability == 'Y') {
    $arFilter['CATALOG_AVAILABLE'] = 'Y';
    $arFilter['ACTIVE_DATE'] = 'Y';
    $arFilter['ACTIVE'] = 'Y';
}

$rsElements = CIBlockElement::GetList(array(), $arFilter, false, $arNavStartParams, $productSelectFields);
while ($arElement = $rsElements->Fetch()) {
    foreach($arElement as $key => &$value) {
        if(is_array($value)) {
            $value = $value[0];
        }
    }

    $rsProps = CIBlockElement::GetProperty($catalogId, $arElement['ID'], array("sort" => "asc"), array('ID' => $catalogAdditionalFields));

    $props = array();
    while($arProp = $rsProps->Fetch()) {
        if(in_array($arProp['ID'], $catalogAdditionalFields) && !empty($arProp['VALUE']['TEXT'])) {
            $arElement['PROPERTY_' . $arProp['ID'] . '_VALUE'] = $arProp['VALUE']['TEXT'];
        }
    }

    $catalogProducts[$arElement['ID']] = $arElement;
}

if(!empty($arNavStartParams)) {
    $shop->appendChild($xml->createElement('version', join('.', array($rsElements->NavPageNomer, $rsElements->NavPageSize, $rsElements->NavPageCount, $rsElements->NavRecordCount))));
}

$currencies = $xml->createElement('currencies');
$shop->appendChild($currencies);

$by = 'name';
$order1 = 'asc';
$rsCurrency = CCurrency::GetList($by, $order1, LANGUAGE_ID);
while ($currency = $rsCurrency->Fetch()) {
    $element = $xml->createElement('currency');
    $element->setAttribute('id', $currency['CURRENCY']);
    $element->setAttribute('rate', $currency['AMOUNT']);
    $currencies->appendChild($element);
}

if(isset($_REQUEST['showCategs'])) {
    $categories = $xml->createElement('categories');
    $shop->appendChild($categories);

    $arFilter = Array('IBLOCK_ID' => $catalogId, 'GLOBAL_ACTIVE' => 'Y');
    $rsSections = CIBlockSection::GetList(array('left_margin' => 'asc'), $arFilter, true);
    while ($arSection = $rsSections->Fetch()) {
        $arSection['NAME'] = mb_convert_encoding($arSection['NAME'], 'HTML-ENTITIES', 'UTF-8');

        $element = $xml->createElement('category');
        $element->appendChild($xml->createCDATASection(html_entity_decode($arSection['NAME'])));
        $element->setAttribute('id', $arSection['ID']);

        if (!empty($arSection['IBLOCK_SECTION_ID'])) {
            $element->setAttribute('parentId', $arSection['IBLOCK_SECTION_ID']);
        }
        $categories->appendChild($element);
    }
}

$offers = $xml->createElement('offers');
$shop->appendChild($offers);

$offerModelFieldId = COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_OFFER_FIELDS_MODEL');
$offerArticleFieldId = COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_OFFER_FIELDS_ARTICLE');
$offerPictureFieldId = COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_OFFER_FIELDS_IMAGE');

$offerSelectFields = array();
if (is_numeric($offerPictureFieldId)) {
    $offerSelectFields[] = 'PROPERTY_' . $offerPictureFieldId;
} elseif (!empty($offerPictureFieldId)) {
    $offerSelectFields[] = $offerPictureFieldId;
}

if (is_numeric($offerModelFieldId)) {
    $offerSelectFields[] = 'PROPERTY_' . $offerModelFieldId;
}

if (is_numeric($offerArticleFieldId)) {
    $offerSelectFields[] = 'PROPERTY_' . $offerArticleFieldId;
}

$offerAdditionalFields = unserialize(COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_OFFER_ADDITIONAL_FIELDS'));
if(!empty($offerAdditionalFields)) {
    foreach ($offerAdditionalFields as $fieldId) {
        $offerSelectFields[] = 'PROPERTY_' . $fieldId;
    }
}

foreach ($catalogProducts as $productId => $arProduct) {
    $offer = $xml->createElement('offer');
    $offers->appendChild($offer);

    $offer->setAttribute('id', $arProduct['ID']);
    $offer->setAttribute('type', 'vendor.model');
    $offer->setAttribute('available', false);

    $price = CPrice::GetBasePrice($arProduct['ID']);

    //$offer->appendChild($xml->createElement('url', '')); //$catalogProducts[$arProduct['ID']]['DETAIL_PAGE_URL']
    if (!empty($arProduct['PURCHASING_PRICE'])) {
        $offer->appendChild($xml->createElement('purchasingPrice', $arProduct['PURCHASING_PRICE']));
        $offer->appendChild($xml->createElement('purchasingCurrency', $arProduct['PURCHASING_CURRENCY']));
    }

    $offer->appendChild($xml->createElement('price', $price['PRICE']));

    $arPrice = CCatalogProduct::GetOptimalPrice($arProduct['ID'], 1, $USER->GetUserGroupArray(), $renewal);
    if($arPrice && (float) $arPrice['DISCOUNT_PRICE'] <> (float) $price['PRICE']) {
        $offer->appendChild($xml->createElement('discountPrice', $arPrice['DISCOUNT_PRICE']));
    }

    $offer->appendChild($xml->createElement('currencyId', $price['CURRENCY']));
    $offer->appendChild($xml->createElement('categoryId', $catalogProducts[$arProduct['ID']]['IBLOCK_SECTION_ID']));

    if(is_numeric($productPictureFieldId)) {
        $picture = !empty($catalogProducts[$arProduct['ID']]['PROPERTY_' . $productPictureFieldId . '_VALUE']) ? 'http://' . COption::GetOptionString('main', 'server_name') . CFile::GetPath($catalogProducts[$arProduct['ID']]['PROPERTY_' . $productPictureFieldId . '_VALUE']) : '';
    } elseif(!empty($productPictureFieldId)) {
        $picture = !empty($catalogProducts[$arProduct['ID']][$productPictureFieldId]) ? 'http://' . COption::GetOptionString('main', 'server_name') . CFile::GetPath($catalogProducts[$arProduct['ID']][$productPictureFieldId]) : '';
    }

    $offer->appendChild($xml->createElement('picture', $picture));
    $offer->appendChild($xml->createElement('vendorCode', $catalogProducts[$arProduct['ID']]['PROPERTY_' . $catalogArticleId . '_VALUE']));

    $element = $xml->createElement('name');
    $element->appendChild($xml->createCDATASection($arProduct['NAME']));
    $offer->appendChild($element);

    if (is_numeric($productModelFieldId)) {
        $element = $xml->createElement('model');
        $element->appendChild($xml->createCDATASection($arProduct['PROPERTY_' . $productModelFieldId . '_VALUE']));
        $offer->appendChild($element);
    }

    if(!empty($catalogAdditionalFields)) {
        foreach ($catalogAdditionalFields as $fieldId) {
            if(is_array($arProduct['PROPERTY_' . $fieldId . '_VALUE'])) {
                $i = 0;
                foreach ($arProduct['PROPERTY_' . $fieldId . '_VALUE'] as $value) {
                    if(empty($value)) {
                        continue;
                    }

                    $element = $xml->createElement('param', $value);
                    $element->setAttribute('name', 'property_' . $fieldId . '_' . $i++);
                    $offer->appendChild($element);
                }
            } else {
                if(empty($arProduct['PROPERTY_' . $fieldId . '_VALUE'])) {
                    continue;
                }

                $value = $arProduct['PROPERTY_' . $fieldId . '_VALUE'];

                // Todo: Compability with old WMS
                $json = json_decode($value, true);
                if(is_array($json) && !empty($json)) {
                    $value = json_decode($value, true);

                    $element = $xml->createElement('param');
                    $element->appendChild($xml->createCDATASection(html_entity_decode(sprintf('102*%s*0', $value['p_id']))));
                    $element->setAttribute('name', 'property_barcode');
                } else {
                    $element = $xml->createElement('param');
                    $element->appendChild($xml->createCDATASection(html_entity_decode($value)));
                    $element->setAttribute('name', 'property_' . $fieldId);
                }

                $offer->appendChild($element);
            }
        }
    }

    if (CCatalogSKU::GetExistOffers($productId)) {
        $arOffersFilter = array();
        if(isset($checkQuantity) && $checkQuantity == 'Y') {
            $arOffersFilter['>CATALOG_QUANTITY'] = 0;
        }

        $arOffers = CCatalogSKU::getOffersList(array($productId), 0, $arOffersFilter, $offerSelectFields);
        foreach($arOffers[$productId] as $arOffer) {
            foreach($arOffer as &$value) {
                if(is_array($value)) {
                    $value = $value[0];
                }
            }

            $name = $arProduct['NAME'];
            $model = $arOffer['PROPERTY_' . $offerModelFieldId . '_VALUE'];
            $article = $arOffer['PROPERTY_' . $offerArticleFieldId . '_VALUE'];

            if(is_numeric($offerPictureFieldId)) {
                $picture = !empty($arOffer['PROPERTY_' . $offerPictureFieldId . '_VALUE']) ? 'http://' . COption::GetOptionString('main', 'server_name') . CFile::GetPath($arOffer['PROPERTY_' . $offerPictureFieldId . '_VALUE']) : '';
            } elseif(!empty($offerPictureFieldId)) {
                $picture = !empty($arOffer[$offerPictureFieldId]) ? 'http://' . COption::GetOptionString('main', 'server_name') . CFile::GetPath($arOffer[$offerPictureFieldId]) : '';
            }

            $offer = $xml->createElement('offer');
            $offers->appendChild($offer);

            $offer->setAttribute('id', $arOffer['ID']);
            $offer->setAttribute('type', 'vendor.model');
            $offer->setAttribute('available', false);

            $price = CPrice::GetBasePrice($arOffer['ID']);

            //$offer->appendChild($xml->createElement('url', '')); //$catalogProducts[$arProduct['ID']]['DETAIL_PAGE_URL']
            if (!empty($arProduct['PURCHASING_PRICE'])) {
                $offer->appendChild($xml->createElement('purchasingPrice', $arProduct['PURCHASING_PRICE']));
                $offer->appendChild($xml->createElement('purchasingCurrency', $arProduct['PURCHASING_CURRENCY']));
            }

            $offer->appendChild($xml->createElement('price', $price['PRICE']));

            $arPrice = CCatalogProduct::GetOptimalPrice($arOffer['ID'], 1, $USER->GetUserGroupArray(), $renewal);
            if($arPrice && (float) $arPrice['DISCOUNT_PRICE'] <> (float) $price['PRICE']) {
                $offer->appendChild($xml->createElement('discountPrice', $arPrice['DISCOUNT_PRICE']));
            }

            $offer->appendChild($xml->createElement('currencyId', $price['CURRENCY']));
            $offer->appendChild($xml->createElement('categoryId', $catalogProducts[$arProduct['ID']]['IBLOCK_SECTION_ID']));

            $imageFieldName = COption::GetOptionString(MODULE_NAME, $prefix . 'ORDERADMIN_CATALOG_FIELDS_IMAGE');
            $imageField = !empty($imageFieldName) ? $imageFieldName : 'PREVIEW_PICTURE';

            $offer->appendChild($xml->createElement('picture', $picture));
            $offer->appendChild($xml->createElement('vendorCode', $article));

            $element = $xml->createElement('name');
            $element->appendChild($xml->createCDATASection($name));
            $offer->appendChild($element);

            $element = $xml->createElement('model');
            $element->appendChild($xml->createCDATASection($model));
            $offer->appendChild($element);

            $element = $xml->createElement('param', $productId);
            $element->setAttribute('name', 'parent');
            $offer->appendChild($element);

            if(!empty($offerAdditionalFields))
            {
                foreach ($offerAdditionalFields as $fieldId) {
                    if(is_array($arOffer['PROPERTY_' . $fieldId . '_VALUE'])) {
                        $i = 0;
                        foreach ($arOffer['PROPERTY_' . $fieldId . '_VALUE'] as $value) {
                            if(empty($value)) {
                                continue;
                            }

                            $element = $xml->createElement('param', '<![CDATA[' . $value . ']]>');
                            $element->setAttribute('name', 'property_' . $fieldId . '_' . $i++);

                            $offer->appendChild($element);
                        }
                    } else {
                        if(empty($arOffer['PROPERTY_' . $fieldId . '_VALUE'])) {
                            continue;
                        }

                        $element = $xml->createElement('param');
                        $element->appendChild($xml->createCDATASection(html_entity_decode($value)));
                        $element->setAttribute('name', 'property_' . $fieldId);

                        $offer->appendChild($element);
                    }
                }
            }
        }
    }
}

header('Content-Type: text/xml; charset=utf-8');
echo $xml->saveXML();
?>