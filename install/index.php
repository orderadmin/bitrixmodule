<?
IncludeModuleLangFile(__FILE__);

Class iqcreative_orderadmin extends CModule
{
    const MODULE_ID = 'iqcreative.orderadmin';
    var $MODULE_ID = 'iqcreative.orderadmin';
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;
    var $strError = '';

    function __construct()
    {
        $arModuleVersion = array();
        include(dirname(__FILE__) . "/version.php");
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_NAME = GetMessage("iqcreative.orderadmin_MODULE_NAME");
        $this->MODULE_DESCRIPTION = GetMessage(
            "iqcreative.orderadmin_MODULE_DESC"
        );

        $this->PARTNER_NAME = GetMessage("iqcreative.orderadmin_PARTNER_NAME");
        $this->PARTNER_URI = GetMessage("iqcreative.orderadmin_PARTNER_URI");
    }

    function InstallDB($arParams = array())
    {
        global $DB;

        RegisterModuleDependences(
            'main', 'OnBuildGlobalMenu', self::MODULE_ID,
            'CIqcreativeOrderadmin', 'OnBuildGlobalMenu'
        );

        $dbRes = $DB->Query(
            "CREATE TABLE IF NOT EXISTS `b_orderadmin_requests` (
		  		`id` int(11) NOT NULL AUTO_INCREMENT,
		  		`order_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
		  		`basket_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
		  		`endpoint` varchar(255) NOT NULL,
		  		`request` text COLLATE utf8_unicode_ci NOT NULL,
		  		`response` text COLLATE utf8_unicode_ci NOT NULL,
				PRIMARY KEY (`id`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;"
        );

        return true;
    }

    function UnInstallDB($arParams = array())
    {
        global $DB;

        UnRegisterModuleDependences(
            'main', 'OnBuildGlobalMenu', self::MODULE_ID,
            'CIqcreativeOrderadmin', 'OnBuildGlobalMenu'
        );

        $DB->Query('DROP TABLE b_orderadmin_requests;');

        return true;
    }

    function InstallFiles($arParams = array())
    {
        CopyDirFiles(
            $_SERVER["DOCUMENT_ROOT"]
            . "/bitrix/modules/iqcreative.orderadmin/install/admin/orderadmin_integration_index.php",
            $_SERVER["DOCUMENT_ROOT"]
            . "/bitrix/admin/orderadmin_integration_index.php", true, true
        );
        CopyDirFiles(
            $_SERVER["DOCUMENT_ROOT"]
            . "/bitrix/modules/iqcreative.orderadmin/install/components",
            $_SERVER["DOCUMENT_ROOT"] . "/bitrix/components", true, true
        );
        CopyDirFiles(
            $_SERVER["DOCUMENT_ROOT"]
            . "/bitrix/modules/iqcreative.orderadmin/install/services/orderadmin",
            $_SERVER["DOCUMENT_ROOT"] . "/bitrix/services/orderadmin", true,
            true
        );
        CopyDirFiles(
            $_SERVER["DOCUMENT_ROOT"]
            . "/bitrix/modules/iqcreative.orderadmin/install/delivery/delivery_orderadmin.php",
            $_SERVER["DOCUMENT_ROOT"]
            . "/bitrix/php_interface/include/sale_delivery/delivery_orderadmin.php",
            true, true
        );
        CopyDirFiles(
            $_SERVER["DOCUMENT_ROOT"]
            . "/bitrix/modules/iqcreative.orderadmin/install/delivery/orderadmin/handler.php",
            $_SERVER["DOCUMENT_ROOT"]
            . "/bitrix/php_interface/include/sale_delivery/orderadmin/handler.php",
            true, true
        );

        return true;
    }

    function UnInstallFiles()
    {
        DeleteDirFilesEx("/bitrix/admin/orderadmin_integration_index.php");
        DeleteDirFilesEx(
            "/bitrix/php_interface/include/sale_delivery/delivery_orderadmin.php"
        );
        DeleteDirFilesEx("/bitrix/components/iqcreative.orderadmin");
        DeleteDirFilesEx("/bitrix/services/orderadmin");
        return true;
    }

    function InstallEvents()
    {
        RegisterModuleDependences(
            "main", "OnEpilog", self::MODULE_ID, "CDeliveryOrderadmin",
            "IncludeScripts"
        );
        RegisterModuleDependences(
            "sale", "OnSaleDeliveryHandlersBuildList", self::MODULE_ID,
            "CDeliveryOrderadmin", "Init"
        );
        return true;

    }

    function UnInstallEvents()
    {
        CModule::IncludeModule('sale');
        UnRegisterModuleDependences(
            "sale", "onSaleDeliveryHandlersBuildList", self::MODULE_ID,
            "CDeliveryOrderadmin", "Init"
        );
        UnRegisterModuleDependences(
            "main", "OnEpilog", self::MODULE_ID, "CDeliveryOrderadmin",
            "IncludeScripts"
        );
        return true;
    }

    function DoInstall()
    {
        global $APPLICATION;
        $this->InstallFiles();
        $this->InstallDB();
        $this->InstallEvents();
        RegisterModule(self::MODULE_ID);
    }

    function DoUninstall()
    {
        global $APPLICATION;
        UnRegisterModule(self::MODULE_ID);
        $this->UnInstallDB();
        $this->UnInstallFiles();
        $this->UnInstallEvents();
    }
}

?>
