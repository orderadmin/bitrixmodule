<?php
/**
 * Created by PhpStorm.
 * User: tasselchof
 * Date: 13.09.15
 * Time: 23:51
 */

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/iqcreative.orderadmin/delivery/orderadmin.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/iqcreative.orderadmin/delivery/orderadminCourier.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/iqcreative.orderadmin/delivery/orderadminServicepoint.php");
