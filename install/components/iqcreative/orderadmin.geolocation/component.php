<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

define('MODULE_NAME', 'iqcreative.orderadmin');

if (!CModule::IncludeModule(MODULE_NAME) || !CModule::IncludeModule('sale')) {
    CHTTP::SetStatus("500 Internal Server Error");
    die('{"error":"Module \"iqcreative.orderadmin\" or module \"sale\" not installed"}');
}

$publicKey = COption::GetOptionString(MODULE_NAME, 'ORDERADMIN_PUBLIC_KEY');
$secret = COption::GetOptionString(MODULE_NAME, 'ORDERADMIN_SECRET');

if (empty($arParams['REMOTE_ADDR'])) {
    $arParams['REMOTE_ADDR'] = $_SERVER['REMOTE_ADDR'];
}

$obCache = new CPHPCache();
$cache_id = MODULE_NAME . "|0.0.1|orderadmin_geolocation|" . $arParams['REMOTE_ADDR'];

if ($obCache->InitCache(60 * 60 * 24 * 30, $cache_id, '/')) {
    $vars = $obCache->GetVars();

    $arResult = $vars['RESULT'];
} else {
    if($obCache->StartDataCache()) {
        $orderadmin = new \Bitrix\Orderadmin\Api($publicKey, $secret);

        $result = $orderadmin->request('GET', '/locations/geo?remoteAddr=' . $arParams['REMOTE_ADDR'])->getResult();
        $result = json_decode($result, true);
        $result = $result['result'];

        $bxLocation = CSaleLocation::GetByZIP($result['locality']['postcode']);

        if (!$bxLocation) {
            $bxLocation = CSaleLocation::GetByZIP($arParams['DEFAULT_POSTCODE']);
        }

        if (isset($arParams['CURRENT_LOCATION'])) {
            $bxLocation = CSaleLocation::GetById($arParams['CURRENT_LOCATION']);
        }

        $result['result']['bxLocation'] = $bxLocation;

        $GLOBALS['ORDERADMIN_LOCATION'] = $result['result'];

        $arResult = $result['result'];

        $obCache->EndDataCache(array(
            "RESULT" => $result['result'],
        ));
    }
}

if (isset($arParams['CURRENT_LOCATION'])) {
    $bxLocation = CSaleLocation::GetById($arParams['CURRENT_LOCATION']);

    $result['result']['bxLocation'] = $bxLocation;

    $arResult = $result['result'];
}


$this->IncludeComponentTemplate();
?>