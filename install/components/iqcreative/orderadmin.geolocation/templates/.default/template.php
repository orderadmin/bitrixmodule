<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (empty($arResult['bxLocation'])): ?>
    <a href="#" title="">
        <?= $arResult['locality']['name']; ?>, <?= $arResult['locality']['area']['name']; ?>
    </a>
<? else: ?>
    <a href="#" title="">
        <?= $arResult['bxLocation']['CITY_NAME']; ?>, <?= $arResult['bxLocation']['REGION_NAME']; ?>
    </a>
<? endif; ?>