$(document).ready(function () {
    $('body').on('click', 'input[type=radio].orderadmin-delivery-option', function(e) {
        $('input[name="DELIVERY_ID"]').val($(this).val());
        submitForm();
    });

    $('body').on('change', 'select.orderadmin-delivery-option', function(e) {
        $('input[name="DELIVERY_ID"]').val($(this).val());
        submitForm();
    });
})