<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
123
<?
$sorted = array();
$sorted['courier'] = $arResult['ORDERADMIN']['courier'];
$sorted['service_point'] = $arResult['ORDERADMIN']['service_point'];
$sorted['russian_post'] = $arResult['ORDERADMIN']['russian_post'];


usort($sorted['courier'], function ($item1, $item2) {
    if ($item1['PRICE'] == $item2['PRICE']) {
        return 0;
    }

    return $item1['PRICE'] < $item2['PRICE'] ? -1 : 1;
});

usort($sorted['service_point'], function ($item1, $item2) {
    if ($item1['PRICE'] == $item2['PRICE']) {
        return 0;
    }

    return $item1['PRICE'] < $item2['PRICE'] ? -1 : 1;
});

$selectedType = false;
foreach ($sorted['courier'] as $profile) {
    if ($profile['CHECKED'] == 'Y') {
        $selectedType = 'courier';
    }
}

foreach ($sorted['russian_post'] as $profile) {
    if ($profile['CHECKED'] == 'Y') {
        $selectedType = 'russian_post';
    }
}

foreach ($sorted['service_point'] as $profile) {
    if ($profile['CHECKED'] == 'Y') {
        $selectedType = 'service_point';
    }
}
?>

<input type="hidden" name="DELIVERY_ID" value="" />

<pre><? print_r($sorted); ?></pre>

<div class="row orderadmin-delivery">
<? foreach($sorted as $type => $types): ?>
    <? if(count($types)): ?>
        <div class="col-xs-<?= 12 / count($arResult['ORDERADMIN']); ?><?= $selectedType == $type ? 'active' : '' ?>'">
            <?
            switch($type) {
                case 'courier':
                    ?>
                    <h3>Курьерской службой</h3>
                    <? foreach($types as $profile): ?>
                        <div class="radio">
                            <label>
                                <input type="radio" name="orderadmin-delivery-option" class="orderadmin-delivery-option" value="orderadmin:<?= $profile['SID']; ?>"<?=$profile["CHECKED"] == "Y" ? " checked=\"checked\"" : "";?>>
                                <b>
                                    <?= $profile['TITLE']; ?>
                                    <div class="pull-right"><?= $profile['PRICE']; ?> ₽</div>
                                </b>
                                <? if($arParams['COURIER']['SHOW_DELIVERY_PERIOD'] == 'Y' && !empty($profile['DATA']['deliveryPeriodMin'])): ?>
                                    <p>Срок доставки: <?= $profile['DATA']['deliveryPeriodMin']; ?><?= !empty($profile['DATA']['deliveryPeriodMax']) ? ' - ' . $profile['DATA']['deliveryPeriodMax'] : ''; ?> дней</p>
                                <? endif; ?>
                                <? if($arParams['COURIER']['SHOW_DELIVERY_PERIOD'] == 'Y' && !empty($profile['DATA']['deliveryPeriodStr'])): ?>
                                    <p>Срок доставки: <?= $profile['DATA']['deliveryPeriodStr']; ?> дней</p>
                                <? endif; ?>
                                <? if($arParams['COURIER']['SHOW_ADDRESS'] == 'Y' && $profile['DATA']['info']['address']): ?>
                                    <p><?= $profile['DATA']['info']['address']; ?></p>
                                <? endif; ?>
                                <? if($arParams['COURIER']['SHOW_PHONE'] == 'Y' && $profile['DATA']['info']['phone']): ?>
                                    <p><?= $profile['DATA']['info']['phone']; ?></p>
                                <? endif; ?>
                                <? if($arParams['COURIER']['SHOW_TIMETABLE'] == 'Y' && $profile['DATA']['info']['timetable']): ?>
                                    <p><b>Режим работы:</b> <?= $profile['DATA']['info']['timetable']; ?></p>
                                <? endif; ?>
                                <? if($arParams['COURIER']['SHOW_DESCRIPTION'] == 'Y' && $profile['DATA']['info']['description']): ?>
                                    <p><?= $profile['DATA']['info']['description']; ?></p>
                                <? endif; ?>
                            </label>
                        </div>
                    <? endforeach; ?>
                    <?
                    break;

                case 'russian_post':
                    ?>
                    <h3>Почта России</h3>
                    <? foreach($types as $profile): ?>
                    <div class="radio">
                        <label>
                            <input type="radio" name="orderadmin-delivery-option" class="orderadmin-delivery-option" value="orderadmin:<?= $profile['SID']; ?>"<?=$profile["CHECKED"] == "Y" ? " checked=\"checked\"" : "";?>>
                            <b>
                                <?= $profile['TITLE']; ?>
                                <div class="pull-right"><?= $profile['PRICE']; ?> ₽</div>
                            </b>
                            <? if($arParams['RUSSIAN_POST']['SHOW_DELIVERY_PERIOD'] == 'Y' && !empty($profile['DATA']['deliveryPeriodMin'])): ?>
                                <p>Срок доставки: <?= $profile['DATA']['deliveryPeriodMin']; ?><?= !empty($profile['DATA']['deliveryPeriodMax']) ? ' - ' . $profile['DATA']['deliveryPeriodMax'] : ''; ?> дней</p>
                            <? endif; ?>
                            <? if($arParams['RUSSIAN_POST']['SHOW_DELIVERY_PERIOD'] == 'Y' && !empty($profile['DATA']['deliveryPeriodStr'])): ?>
                                <p>Срок доставки: <?= $profile['DATA']['deliveryPeriodStr']; ?> дней</p>
                            <? endif; ?>
                            <? if($arParams['RUSSIAN_POST']['SHOW_ADDRESS'] == 'Y' && $profile['DATA']['info']['address']): ?>
                                <p><?= $profile['DATA']['info']['address']; ?></p>
                            <? endif; ?>
                            <? if($arParams['RUSSIAN_POST']['SHOW_PHONE'] == 'Y' && $profile['DATA']['info']['phone']): ?>
                                <p><?= $profile['DATA']['info']['phone']; ?></p>
                            <? endif; ?>
                            <? if($arParams['RUSSIAN_POST']['SHOW_TIMETABLE'] == 'Y' && $profile['DATA']['info']['timetable']): ?>
                                <p><b>Режим работы:</b> <?= $profile['DATA']['info']['timetable']; ?></p>
                            <? endif; ?>
                            <? if($arParams['RUSSIAN_POST']['SHOW_DESCRIPTION'] == 'Y' && $profile['DATA']['info']['description']): ?>
                                <p><?= $profile['DATA']['info']['description']; ?></p>
                            <? endif; ?>
                        </label>
                    </div>
                    <? endforeach; ?>
                    <?
                    break;

                case 'service_point':
                    ?>
                    <h3>Пункты выдачи заказов</h3>
                    <? if(count($types) > 1): ?>
                        <? if($arParams['SERVICE_POINTS_VIEW'] == 'map'): ?>
                        <div class="select">
                            <label>
                                <select class="form-control orderadmin-delivery-option">
                                    <option value="">Выбрать...</option>
                                    <? foreach($types as $profile): ?>
                                        <?
                                        if($profile["CHECKED"] == "Y") {
                                            $checkedProfile = $profile;
                                        }
                                        ?>
                                    <option value="orderadmin:<?= $profile['SID']; ?>" <?=$profile["CHECKED"] == "Y" ? " selected" : "";?>><?= $profile['TITLE']; ?> - <?= $profile['PRICE']; ?> ₽</option>
                                        <!--
                                        <? if($arParams['SERVICE_POINT']['SHOW_DELIVERY_PERIOD'] == 'Y' && !empty($profile['DATA']['deliveryPeriodMin'])): ?>
                                            <p>Срок доставки: <?= $profile['DATA']['deliveryPeriodMin']; ?><?= !empty($profile['DATA']['deliveryPeriodMax']) ? ' - ' . $profile['DATA']['deliveryPeriodMax'] : ''; ?> дней</p>
                                        <? endif; ?>
                                        <? if($arParams['SERVICE_POINT']['SHOW_DELIVERY_PERIOD'] == 'Y' && !empty($profile['DATA']['deliveryPeriodStr'])): ?>
                                            <p>Срок доставки: <?= $profile['DATA']['deliveryPeriodStr']; ?> дней</p>
                                        <? endif; ?>
                                        <? if($arParams['SERVICE_POINT']['SHOW_ADDRESS'] == 'Y' && $profile['DATA']['info']['address']): ?>
                                            <p><?= $profile['DATA']['info']['address']; ?></p>
                                        <? endif; ?>
                                        <? if($arParams['SERVICE_POINT']['SHOW_PHONE'] == 'Y' && $profile['DATA']['info']['phone']): ?>
                                            <p><?= $profile['DATA']['info']['phone']; ?></p>
                                        <? endif; ?>
                                        <? if($arParams['SERVICE_POINT']['SHOW_TIMETABLE'] == 'Y' && $profile['DATA']['info']['timetable']): ?>
                                            <p><b>Режим работы:</b> <?= $profile['DATA']['info']['timetable']; ?></p>
                                        <? endif; ?>
                                        <? if($arParams['SERVICE_POINT']['SHOW_DESCRIPTION'] == 'Y' && $profile['DATA']['info']['description']): ?>
                                            <p><?= $profile['DATA']['info']['description']; ?></p>
                                        <? endif; ?>
                                        -->
                                    <? endforeach; ?>
                                </select>
                            </label>
                        </div>

                        <? if($checkedProfile): ?>
                            <? $profile = $checkedProfile; ?>
                            <div class="orderadmin-info">
                                <h4>
                                    <?= $profile['TITLE']; ?>
                                </h4>
                                <? if($arParams['SERVICE_POINT']['SHOW_DELIVERY_PERIOD'] == 'Y' && !empty($profile['DATA']['deliveryPeriodMin'])): ?>
                                    <p>Срок доставки: <?= $profile['DATA']['deliveryPeriodMin']; ?><?= !empty($profile['DATA']['deliveryPeriodMax']) ? ' - ' . $profile['DATA']['deliveryPeriodMax'] : ''; ?> дней</p>
                                <? endif; ?>
                                <? if($arParams['SERVICE_POINT']['SHOW_DELIVERY_PERIOD'] == 'Y' && !empty($profile['DATA']['deliveryPeriodStr'])): ?>
                                    <p>Срок доставки: <?= $profile['DATA']['deliveryPeriodStr']; ?> дней</p>
                                <? endif; ?>
                                <? if($arParams['SERVICE_POINT']['SHOW_ADDRESS'] == 'Y' && $profile['DATA']['info']['address']): ?>
                                    <p><?= $profile['DATA']['info']['address']; ?></p>
                                <? endif; ?>
                                <? if($arParams['SERVICE_POINT']['SHOW_PHONE'] == 'Y' && $profile['DATA']['info']['phone']): ?>
                                    <p><?= $profile['DATA']['info']['phone']; ?></p>
                                <? endif; ?>
                                <? if($arParams['SERVICE_POINT']['SHOW_TIMETABLE'] == 'Y' && $profile['DATA']['info']['timetable']): ?>
                                    <p><b>Режим работы:</b> <?= $profile['DATA']['info']['timetable']; ?></p>
                                <? endif; ?>
                                <? if($arParams['SERVICE_POINT']['SHOW_DESCRIPTION'] == 'Y' && $profile['DATA']['info']['description']): ?>
                                    <p><?= $profile['DATA']['info']['description']; ?></p>
                                <? endif; ?>
                                <? if($arParams['SERVICE_POINT']['SHOW_MAP'] == 'Y' && $profile['DATA']['info']['geo']): ?>
                                    <p>
                                        <?
                                        $geo = explode(',', $profile['DATA']['info']['geo']);
                                        ?>
                                        <img src="https://static-maps.yandex.ru/1.x/?ll=<?=join(',', array($geo[1], $geo[0]));?>&size=350,250&z=14&l=map&pt=<?=join(',', array($geo[1], $geo[0]));?>,pmwtm1~37.64,55.76363,pmwtm99" />
                                    </p>
                                <? endif; ?>
                                <h4>Стоимость доставки: <?= $profile['PRICE']; ?> ₽</h4>
                            </div>
                        <? endif; ?>
                        <? else: ?>
                            <? foreach($types as $profile): ?>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="orderadmin-delivery-option" class="orderadmin-delivery-option" value="orderadmin:<?= $profile['SID']; ?>"<?=$profile["CHECKED"] == "Y" ? " checked=\"checked\"" : "";?>>
                                    <b>
                                        <?= $profile['TITLE']; ?>
                                        <div class="pull-right"><?= $profile['PRICE']; ?> ₽</div>
                                    </b>
                                    <? if($arParams['SERVICE_POINT']['SHOW_DELIVERY_PERIOD'] == 'Y' && !empty($profile['DATA']['deliveryPeriodMin'])): ?>
                                        <p>Срок доставки: <?= $profile['DATA']['deliveryPeriodMin']; ?><?= !empty($profile['DATA']['deliveryPeriodMax']) ? ' - ' . $profile['DATA']['deliveryPeriodMax'] : ''; ?> дней</p>
                                    <? endif; ?>
                                    <? if($arParams['SERVICE_POINT']['SHOW_DELIVERY_PERIOD'] == 'Y' && !empty($profile['DATA']['deliveryPeriodStr'])): ?>
                                        <p>Срок доставки: <?= $profile['DATA']['deliveryPeriodStr']; ?> дней</p>
                                    <? endif; ?>
                                    <? if($arParams['SERVICE_POINT']['SHOW_ADDRESS'] == 'Y' && $profile['DATA']['info']['address']): ?>
                                        <p><?= $profile['DATA']['info']['address']; ?></p>
                                    <? endif; ?>
                                    <? if($arParams['SERVICE_POINT']['SHOW_PHONE'] == 'Y' && $profile['DATA']['info']['phone']): ?>
                                        <p><?= $profile['DATA']['info']['phone']; ?></p>
                                    <? endif; ?>
                                    <? if($arParams['SERVICE_POINT']['SHOW_TIMETABLE'] == 'Y' && $profile['DATA']['info']['timetable']): ?>
                                        <p><b>Режим работы:</b> <?= $profile['DATA']['info']['timetable']; ?></p>
                                    <? endif; ?>
                                    <? if($arParams['SERVICE_POINT']['SHOW_DESCRIPTION'] == 'Y' && $profile['DATA']['info']['description']): ?>
                                        <p><?= $profile['DATA']['info']['description']; ?></p>
                                    <? endif; ?>
                                </label>
                            </div>
                            <? endforeach; ?>
                        <? endif; ?>
                    <? else: ?>
                        <? $profile = $types[0]; ?>
                            <div class="orderadmin-info">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="orderadmin-delivery-option" class="orderadmin-delivery-option" value="orderadmin:<?= $profile['SID']; ?>"<?=$profile["CHECKED"] == "Y" ? " checked=\"checked\"" : "";?>>
                                        <b>
                                            <?= $profile['TITLE']; ?>
                                            <div class="pull-right"><?= $profile['PRICE']; ?> ₽</div>
                                        </b>
                                        <? if($arParams['SERVICE_POINT']['SHOW_DELIVERY_PERIOD'] == 'Y' && !empty($profile['DATA']['deliveryPeriodMin'])): ?>
                                            <p>Срок доставки: <?= $profile['DATA']['deliveryPeriodMin']; ?><?= !empty($profile['DATA']['deliveryPeriodMax']) ? ' - ' . $profile['DATA']['deliveryPeriodMax'] : ''; ?> дней</p>
                                        <? endif; ?>
                                        <? if($arParams['SERVICE_POINT']['SHOW_DELIVERY_PERIOD'] == 'Y' && !empty($profile['DATA']['deliveryPeriodStr'])): ?>
                                            <p>Срок доставки: <?= $profile['DATA']['deliveryPeriodStr']; ?> дней</p>
                                        <? endif; ?>
                                        <? if($arParams['SERVICE_POINT']['SHOW_ADDRESS'] == 'Y' && $profile['DATA']['info']['address']): ?>
                                            <p><?= $profile['DATA']['info']['address']; ?></p>
                                        <? endif; ?>
                                        <? if($arParams['SERVICE_POINT']['SHOW_PHONE'] == 'Y' && $profile['DATA']['info']['phone']): ?>
                                            <p><?= $profile['DATA']['info']['phone']; ?></p>
                                        <? endif; ?>
                                        <? if($arParams['SERVICE_POINT']['SHOW_TIMETABLE'] == 'Y' && $profile['DATA']['info']['timetable']): ?>
                                            <p><b>Режим работы:</b> <?= $profile['DATA']['info']['timetable']; ?></p>
                                        <? endif; ?>
                                        <? if($arParams['SERVICE_POINT']['SHOW_DESCRIPTION'] == 'Y' && $profile['DATA']['info']['description']): ?>
                                            <p><?= $profile['DATA']['info']['description']; ?></p>
                                        <? endif; ?>
                                    </label>
                                </div>
                                <? if($arParams['SERVICE_POINT']['SHOW_MAP'] == 'Y' && $profile['DATA']['info']['geo']): ?>
                                    <p>
                                        <?
                                        $geo = explode(',', $profile['DATA']['info']['geo']);
                                        ?>
                                        <img src="https://static-maps.yandex.ru/1.x/?ll=<?=join(',', array($geo[1], $geo[0]));?>&size=350,250&z=14&l=map&pt=<?=join(',', array($geo[1], $geo[0]));?>,pmwtm1~37.64,55.76363,pmwtm99" />
                                    </p>
                                <? endif; ?>
                                <h4>Стоимость доставки: <?= $profile['PRICE']; ?> ₽</h4>
                            </div>
                    <? endif; ?>
                    <?
                    break;
            }
            ?>
        </div>
    <? endif; ?>
<? endforeach; ?>
</div>