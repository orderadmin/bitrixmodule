<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?
$sorted = array();
$sorted['courier'] = $arResult['ORDERADMIN']['courier'];
$sorted['russian_post'] = $arResult['ORDERADMIN']['russian_post'];
$sorted['service_point'] = $arResult['ORDERADMIN']['service_point'];

usort($sorted['courier'], function ($item1, $item2) {
    if ($item1['PRICE'] == $item2['PRICE']) {
        return 0;
    }

    return $item1['PRICE'] < $item2['PRICE'] ? -1 : 1;
});

usort($sorted['service_point'], function ($item1, $item2) {
    if ($item1['PRICE'] == $item2['PRICE']) {
        return 0;
    }

    return $item1['PRICE'] < $item2['PRICE'] ? -1 : 1;
});

$id = explode(':', $arParams['DELIVERY_ID']);
$id = $id[1];

$selectedType = false;
foreach ($sorted['courier'] as $profile) {
    if($profile['SID'] == $id) {
        $sorted['courier'][$key]['CHECKED'] = 'Y';
    }

    if ($profile['CHECKED'] == 'Y') {
        $selectedType = 'courier';
    }
}

foreach ($sorted['russian_post'] as $key => $profile) {
    if($profile['SID'] == $id) {
        $sorted['russian_post'][$key]['CHECKED'] = 'Y';
    }

    if ($profile['CHECKED'] == 'Y') {
        $selectedType = 'russian_post';
    }
}

foreach ($sorted['service_point'] as $profile) {
    if($profile['SID'] == $id) {
        $sorted['service_point'][$key]['CHECKED'] = 'Y';
    }

    if ($profile['CHECKED'] == 'Y') {
        $selectedType = 'service_point';
    }
}
?>

<input type="hidden" name="DELIVERY_ID" value="<?= $arParams['DELIVERY_ID'] ?>"/>

<? ob_start(); ?>
<div class="row">
    <?
    if (count(array_filter($sorted)) == 0) {
        echo '<p>Рассчет доставки временно не доступен.</p>';
    }
    ?>
    <? foreach ($sorted as $type => $types): ?>
        <? if (count($types)): ?>
            <div id="<?= $type; ?>" class="col-md-12 orderadmin-tab<? if ($selectedType == $type): ?>active<? endif; ?>">
                <?
                switch ($type) {
                    case 'courier':
                        ?>
                        <h3>Курьерской службой</h3>
                        <? $i = 0; ?>
                        <? foreach ($types as $profile): ?>
                        <?
                        if ($profile["CHECKED"] == "Y") {
                            $checkedProfile = $profile;
                            $checkedType = $type;
                        }
                        ?>
                        <div class="radio col-xs-4">
                            <label>
                                <input type="radio" name="orderadmin-delivery-option"
                                       class="orderadmin-delivery-option"
                                       value="orderadmin:<?= $profile['SID']; ?>"<?= $profile["CHECKED"] == "Y" ? " checked=\"checked\"" : ""; ?>>
                                <h4><?= $profile['DATA']['info']['delivery-service']['name']; ?>
                                    : <?= $profile['TITLE']; ?></h4>
                                <? if ($arParams['COURIER']['SHOW_DELIVERY_PERIOD'] == 'Y' && !empty($profile['DATA']['deliveryPeriodMin'])): ?>
                                    <p>Срок доставки
                                        (дней): <?= $profile['DATA']['deliveryPeriodMin']; ?><?= !empty($profile['DATA']['deliveryPeriodMax']) && $profile['DATA']['deliveryPeriodMax'] > $profile['DATA']['deliveryPeriodMin'] ? ' - ' . $profile['DATA']['deliveryPeriodMax'] : ''; ?></p>
                                <? endif; ?>
                                <? if ($arParams['COURIER']['SHOW_DELIVERY_PERIOD'] == 'Y' && !empty($profile['DATA']['deliveryPeriodStr'])): ?>
                                    <p>Срок доставки (дней): <?= $profile['DATA']['deliveryPeriodStr']; ?></p>
                                <? endif; ?>
                                <? if ($arParams['COURIER']['SHOW_ADDRESS'] == 'Y' && $profile['DATA']['info']['address']): ?>
                                    <p><?= $profile['DATA']['info']['address']; ?></p>
                                <? endif; ?>
                                <? if ($arParams['COURIER']['SHOW_PHONE'] == 'Y' && $profile['DATA']['info']['phone']): ?>
                                    <p><?= $profile['DATA']['info']['phone']; ?></p>
                                <? endif; ?>
                                <? if ($arParams['COURIER']['SHOW_TIMETABLE'] == 'Y' && $profile['DATA']['info']['timetable']): ?>
                                    <p><b>Режим работы:</b> <?= $profile['DATA']['info']['timetable']; ?></p>
                                <? endif; ?>
                                <? if ($arParams['COURIER']['SHOW_DESCRIPTION'] == 'Y' && $profile['DATA']['info']['description']): ?>
                                    <p><?= $profile['DATA']['info']['description']; ?></p>
                                <? endif; ?>
                                <div style="font-weight: bold;"><?= $profile['PRICE']; ?> ₽</div>
                            </label>
                        </div>
                        <? $i += 4; ?>
                        <? if ($i % 12 === false): ?>
                            <div class="clearing"></div>
                        <? endif; ?>
                    <? endforeach; ?>
                        <?
                        break;

                    case 'service_point':
                        ?>
                        <h3>Пункты выдачи заказов</h3>
                        <? if (count($types) > 1): ?>
                        <? if ($arParams['SERVICE_POINTS_VIEW'] == 'map'): ?>
                            <div class="select">
                                <label>
                                    <select class="form-control orderadmin-delivery-option" style="width:100%;">
                                        <option value="">Выбрать...</option>
                                        <? $curName = null; ?>
                                        <? foreach ($types as $profile): ?>
                                            <? if($curName != $profile['DATA']['info']['delivery-service']['name']): ?>
                                                <? if(!is_null($curName)) { echo '</optgroup>'; } ?>
                                                <optgroup label="<?= $profile['DATA']['info']['delivery-service']['name']; ?>">
                                                <? $curName = $profile['DATA']['info']['delivery-service']['name']; ?>
                                            <? endif; ?>
                                                <?
                                                if ($profile["CHECKED"] == "Y") {
                                                    $checkedProfile = $profile;
                                                    $checkedType = $type;
                                                }
                                                ?>
                                                <option
                                                    value="orderadmin:<?= $profile['SID']; ?>" <?= $profile["CHECKED"] == "Y" ? " selected" : ""; ?>>
                                                    <?= !empty($profile['DATA']['info']['service-point']['name']) ? $profile['DATA']['info']['service-point']['name'] : $profile['DATA']['name']; ?> - <?= $profile['PRICE']; ?> ₽
                                                </option>
                                        <? endforeach; ?>
                                        </optgroup>
                                    </select>
                                </label>
                            </div>

                            <? if ($checkedProfile && $type == 'service_point'): ?>
                                <? $profile = $checkedProfile; ?>
                                <div class="orderadmin-info row">
                                    <div class="col-xs-5">
                                        <? if ($arParams['SERVICE_POINT']['SHOW_MAP'] == 'Y' && $profile['DATA']['info']['service-point']['geo']): ?>
                                            <?
                                            $geo = explode(',', $profile['DATA']['info']['service-point']['geo']);
                                            ?>
                                            <img
                                                src="https://static-maps.yandex.ru/1.x/?ll=<?= trim(join(',', array($geo[1], $geo[0]))); ?>&size=350,250&z=14&l=map&pt=<?= trim(join(',', array($geo[1], $geo[0]))); ?>,pmwtm1~37.64,55.76363,pmwtm99"/>
                                        <? endif; ?>
                                    </div>
                                    <div class="col-xs-7">
                                        <? if ($arParams['SERVICE_POINT']['SHOW_DELIVERY_PERIOD'] == 'Y' && !empty($profile['DATA']['deliveryPeriodMin'])): ?>
                                            <p>Срок доставки
                                                (дней): <?= $profile['DATA']['deliveryPeriodMin']; ?><?= !empty($profile['DATA']['deliveryPeriodMax']) && $profile['DATA']['deliveryPeriodMax'] > $profile['DATA']['deliveryPeriodMin'] ? ' - ' . $profile['DATA']['deliveryPeriodMax'] : ''; ?></p>
                                        <? endif; ?>
                                        <? if ($arParams['SERVICE_POINT']['SHOW_DELIVERY_PERIOD'] == 'Y' && !empty($profile['DATA']['deliveryPeriodStr'])): ?>
                                            <p>Срок доставки (дней): <?= $profile['DATA']['deliveryPeriodStr']; ?></p>
                                        <? endif; ?>
                                        <? if ($arParams['SERVICE_POINT']['SHOW_ADDRESS'] == 'Y' && $profile['DATA']['info']['service-point']['address']): ?>
                                            <p><?= $profile['DATA']['info']['service-point']['address']; ?></p>
                                        <? endif; ?>
                                        <? if ($arParams['SERVICE_POINT']['SHOW_PHONE'] == 'Y' && $profile['DATA']['info']['service-point']['phone']): ?>
                                            <p><?= $profile['DATA']['info']['service-point']['phone']; ?></p>
                                        <? endif; ?>
                                        <? if ($arParams['SERVICE_POINT']['SHOW_TIMETABLE'] == 'Y' && $profile['DATA']['info']['service-point']['timetable']): ?>
                                            <p><b>Режим
                                                    работы:</b> <?= $profile['DATA']['info']['service-point']['timetable']; ?>
                                            </p>
                                        <? endif; ?>
                                        <? if ($arParams['SERVICE_POINT']['SHOW_DESCRIPTION'] == 'Y' && $profile['DATA']['info']['service-point']['description']): ?>
                                            <p><?= $profile['DATA']['info']['service-point']['description']; ?></p>
                                        <? endif; ?>
                                        <h4>Стоимость доставки: <?= $profile['PRICE']; ?> ₽</h4>
                                    </div>
                                </div>
                            <? endif; ?>
                        <? else: ?>
                            <? foreach ($types as $profile): ?>
                                <?
                                if ($profile["CHECKED"] == "Y") {
                                    $checkedProfile = $profile;
                                    $checkedType = $type;
                                }
                                ?>
                                <div class="radio col-xs-4">
                                    <label>
                                        <input type="radio" name="orderadmin-delivery-option"
                                               class="orderadmin-delivery-option"
                                               value="orderadmin:<?= $profile['SID']; ?>"<?= $profile["CHECKED"] == "Y" ? " checked=\"checked\"" : ""; ?>>

                                        <h3><?= $profile['TITLE']; ?></h3>
                                        <? if ($arParams['SERVICE_POINT']['SHOW_DELIVERY_PERIOD'] == 'Y' && !empty($profile['DATA']['deliveryPeriodMin'])): ?>
                                            <p>Срок доставки
                                                (дней): <?= $profile['DATA']['deliveryPeriodMin']; ?><?= !empty($profile['DATA']['deliveryPeriodMax']) && $profile['DATA']['deliveryPeriodMax'] > $profile['DATA']['deliveryPeriodMin'] ? ' - ' . $profile['DATA']['deliveryPeriodMax'] : ''; ?></p>
                                        <? endif; ?>
                                        <? if ($arParams['SERVICE_POINT']['SHOW_DELIVERY_PERIOD'] == 'Y' && !empty($profile['DATA']['deliveryPeriodStr'])): ?>
                                            <p>Срок доставки (дней): <?= $profile['DATA']['deliveryPeriodStr']; ?></p>
                                        <? endif; ?>
                                        <? if ($arParams['SERVICE_POINT']['SHOW_ADDRESS'] == 'Y' && $profile['DATA']['info']['service-point']['address']): ?>
                                            <p><?= $profile['DATA']['info']['service-point']['address']; ?></p>
                                        <? endif; ?>
                                        <? if ($arParams['SERVICE_POINT']['SHOW_PHONE'] == 'Y' && $profile['DATA']['info']['service-point']['phone']): ?>
                                            <p><?= $profile['DATA']['info']['service-point']['phone']; ?></p>
                                        <? endif; ?>
                                        <? if ($arParams['SERVICE_POINT']['SHOW_TIMETABLE'] == 'Y' && $profile['DATA']['info']['service-point']['timetable']): ?>
                                            <p><b>Режим
                                                    работы:</b> <?= $profile['DATA']['info']['service-point']['timetable']; ?>
                                            </p>
                                        <? endif; ?>
                                        <? if ($arParams['SERVICE_POINT']['SHOW_DESCRIPTION'] == 'Y' && $profile['DATA']['info']['service-point']['description']): ?>
                                            <p><?= $profile['DATA']['info']['service-point']['description']; ?></p>
                                        <? endif; ?>
                                        <div style="font-weight: bold;">Цена: <?= $profile['PRICE']; ?> ₽</div>
                                    </label>
                                </div>
                            <? endforeach; ?>
                        <? endif; ?>
                    <? else: ?>
                        <? $profile = $types[0]; ?>
                        <div class="orderadmin-info col-xs-12">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="orderadmin-delivery-option"
                                           class="orderadmin-delivery-option"
                                           value="orderadmin:<?= $profile['SID']; ?>"<?= $profile["CHECKED"] == "Y" ? " checked=\"checked\"" : ""; ?>>
                                    <h4><?= $profile['TITLE']; ?></h4>
                                    <? if ($arParams['SERVICE_POINT']['SHOW_DELIVERY_PERIOD'] == 'Y' && !empty($profile['DATA']['deliveryPeriodMin'])): ?>
                                        <p>Срок доставки
                                            (дней): <?= $profile['DATA']['deliveryPeriodMin']; ?><?= !empty($profile['DATA']['deliveryPeriodMax']) && $profile['DATA']['deliveryPeriodMax'] > $profile['DATA']['deliveryPeriodMin'] ? ' - ' . $profile['DATA']['deliveryPeriodMax'] : ''; ?></p>
                                    <? endif; ?>
                                    <? if ($arParams['SERVICE_POINT']['SHOW_DELIVERY_PERIOD'] == 'Y' && !empty($profile['DATA']['deliveryPeriodStr'])): ?>
                                        <p>Срок доставки (дней): <?= $profile['DATA']['deliveryPeriodStr']; ?></p>
                                    <? endif; ?>
                                    <? if ($arParams['SERVICE_POINT']['SHOW_ADDRESS'] == 'Y' && $profile['DATA']['info']['address']): ?>
                                        <p><?= $profile['DATA']['info']['address']; ?></p>
                                    <? endif; ?>
                                    <? if ($arParams['SERVICE_POINT']['SHOW_PHONE'] == 'Y' && $profile['DATA']['info']['phone']): ?>
                                        <p><?= $profile['DATA']['info']['phone']; ?></p>
                                    <? endif; ?>
                                    <? if ($arParams['SERVICE_POINT']['SHOW_TIMETABLE'] == 'Y' && $profile['DATA']['info']['timetable']): ?>
                                        <p><b>Режим работы:</b> <?= $profile['DATA']['info']['timetable']; ?></p>
                                    <? endif; ?>
                                    <? if ($arParams['SERVICE_POINT']['SHOW_DESCRIPTION'] == 'Y' && $profile['DATA']['info']['description']): ?>
                                        <p><?= $profile['DATA']['info']['description']; ?></p>
                                    <? endif; ?>
                                    <div style="font-weight:bold;"><?= $profile['PRICE']; ?> ₽</div>
                                </label>
                            </div>
                            <? if ($arParams['SERVICE_POINT']['SHOW_MAP'] == 'Y' && $profile['DATA']['info']['service-point']['geo']): ?>
                                <p>
                                    <?
                                    $geo = explode(',', $profile['DATA']['info']['service-point']['geo']);
                                    ?>
                                    <img
                                        src="https://static-maps.yandex.ru/1.x/?ll=<?= join(',', array($geo[1], $geo[0])); ?>&size=350,250&z=14&l=map&pt=<?= join(',', array($geo[1], $geo[0])); ?>,pmwtm1~37.64,55.76363,pmwtm99"/>
                                </p>
                            <? endif; ?>
                            <h4>Стоимость доставки: <?= $profile['PRICE']; ?> ₽</h4>
                        </div>
                    <? endif; ?>
                        <?
                        break;
                }
                ?>
            </div>
        <? endif; ?>
    <? endforeach; ?>
</div>
<? $res = ob_get_clean(); ?>

<div class="orderform">
    <h3>Способ доставки <span class="bx_sof_req">*</span></h3>

    <div class="row delivery">
        <? if (count($sorted['courier']) > 0): ?>
            <div
                class="col-xs-<?= 12 / count($arResult['ORDERADMIN']); ?><? if ($checkedType == 'courier'): ?> checked<? endif; ?>">
                <a href="#courier" id="tab_courier">Доставка</a>
            </div>
        <? endif; ?>

        <? if (count($sorted['service_point']) > 0): ?>
            <div
                class="col-xs-<?= 12 / count($arResult['ORDERADMIN']); ?><? if ($checkedType == 'service_point'): ?> checked<? endif; ?>">
                <a href="#service_point" id="tab_service_point">Самовывоз</a>
            </div>
        <? endif; ?>

        <? if (count($sorted['russian_post']) > 0): ?>
            <div
                class="col-xs-<?= 12 / count($arResult['ORDERADMIN']); ?><? if ($checkedType == 'russian_post'): ?> checked<? endif; ?>">
                <a href="#russian_post" id="tab_russian_post">Почта России</a>
            </div>
        <? endif; ?>
    </div>
</div>

<div class="orderform orderadmin-delivery"
     id="orderadmin"<? if (!empty($checkedType)): ?> style="display: block;"<? endif; ?>>
    <?= $res; ?>
</div>