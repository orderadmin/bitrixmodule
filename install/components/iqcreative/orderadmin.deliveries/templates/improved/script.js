$(document).ready(function () {
    $('body').on('click', 'input[type=radio].orderadmin-delivery-option', function(e) {
        $('input[name="DELIVERY_ID"]').val($(this).val());
        submitForm();
    });

    $('body').on('change', 'select.orderadmin-delivery-option', function(e) {
        $('input[name="DELIVERY_ID"]').val($(this).val());
        submitForm();
    });

    $('body').on('click', '.delivery > div a', function(e) {
        $(this).parents('div.row').find('> div').removeClass('checked');
        $(this).parents('div').addClass('checked');
        $('.orderadmin-delivery').show();
        $('.orderadmin-delivery div.row > div').hide();
        $('.orderadmin-delivery div.row > div' + $(this).attr('href')).show();
        e.preventDefault();
    });
})