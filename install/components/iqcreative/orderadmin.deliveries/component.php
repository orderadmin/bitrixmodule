<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

define('MODULE_NAME', 'iqcreative.orderadmin');

$arResult['DELIVERIES'] = $arParams['DELIVERIES'];

$arResult['ORDERADMIN'] = array();
foreach($arResult['DELIVERIES']['orderadmin']['PROFILES'] as $profile) {
    $arResult['ORDERADMIN'][$profile['DATA']['type']][] = $profile;
}

$this->IncludeComponentTemplate();
?>