<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?
$pvz = array();
$courierService = array();
$minCourierService = false;

foreach($arResult as $service) {
    if($service['type'] == 'courier' || $service['type'] == 'russian_post') {
        if(!$minCourierService || ($minCourierService['price'] > 0 && $minCourierService['price'] > $service['price'])) {
            $minCourierService = $service;
        }

        $courierService[] = $service['name'] . ' за ' . $service['price'] . ' р.';
    } else {
        $pvz[] = $service['name'];
    }
}
?>

<div class="delivery">
    <? if(count($courierService) > 0): ?>
    <p class="text">минимальная стоимость доставки</p>
    <p class="value">
        <a href="javascript: void(0)" data-toggle="popover" data-placement="top"
                        title="Пункты выдачи заказов"
                        data-content="<?= $minCourierService['name']; ?>"><?= $minCourierService['price']; ?></a><span class="rur cur">р<span>уб.</span></span></p>
    <? endif; ?>

    <div class="clear"></div>

    <p class="text">самовывоз доступен</p>
    <p class="value">
        <? if(count(array_filter($pvz)) > 0): ?>
        <a href="javascript: void(0)" data-toggle="popover" data-placement="top"
                        title="Пункты выдачи заказов"
                        data-content="<?= join(', ', array_filter($pvz)) ;?>">Из пунктов ПВЗ</a>
        <? else: ?>
            <a href="javascript: void(0)" data-toggle="popover" data-placement="top"
               title="Пункты выдачи заказов"
               data-content="К сожалению, в вашем регионе пока нет пунктов выдачи заказов">Не доступен</a>
        <? endif; ?>
    </p>
</div>