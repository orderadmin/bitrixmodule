<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

define('MODULE_NAME', 'iqcreative.orderadmin');

if (!CModule::IncludeModule(MODULE_NAME) || !CModule::IncludeModule('sale')) {
    CHTTP::SetStatus("500 Internal Server Error");
    die('{"error":"Module \"iqcreative.orderadmin\" or module \"sale\" not installed"}');
}

$arResult = array();

$obCache = new CPHPCache();
$cache_id = MODULE_NAME . "|0.0.3|delivery_services|toCity" . $arParams['GEOLOCATION_CITY'] . '|' . join('|', $arParams['CALCULATION']);

$cache = $obCache->InitCache(60 * 60 * 24 * 30, $cache_id, '/');

if ($cache) {
    $vars = $obCache->GetVars();

    $params = $vars['PARAMS'];
    $deliveryResult = $vars['RESULT'];
}

if(empty($deliveryResult)) {
    if($obCache->StartDataCache()) {
        $publicKey = COption::GetOptionString(MODULE_NAME, 'ORDERADMIN_PUBLIC_KEY');
        $secret = COption::GetOptionString(MODULE_NAME, 'ORDERADMIN_SECRET');
        $country = COption::GetOptionString(MODULE_NAME, 'ORDERADMIN_BASE_COUNTRY');
        $baseWeight = COption::GetOptionString(MODULE_NAME, 'ORDERADMIN_BASE_WEIGHT');
        $warehouse = COption::GetOptionString(MODULE_NAME, 'ORDERADMIN_WAREHOUSE');
        $deliveryServices = COption::GetOptionString(MODULE_NAME, 'ORDERADMIN_DELIVERY_SERVICES');
        $prepaymentServices = unserialize(COption::GetOptionString(MODULE_NAME, 'ORDERADMIN_PREPAYMENT_SERVICES'));

        $orderadmin = new \Bitrix\Orderadmin\Api($publicKey, $secret);

        if ($arParams['GEOLOCATION_CITY']) {
            $item = \Bitrix\Sale\Location\LocationTable::getById($arParams['GEOLOCATION_CITY'])->fetch();

            $item = \Bitrix\Sale\Location\LocationTable::getByCode($item['CODE'], array(
                'filter' => array('=NAME.LANGUAGE_ID' => LANGUAGE_ID),
                'select' => array('*', 'NAME_RU' => 'NAME.NAME')
            ))->fetch();

            $res = $orderadmin->request('GET', '/locations/' . $country . '?queryString=' . urlencode($item['NAME_RU']));
            $res = json_decode($res->getResult());

            if (count($res->result) == 1) {
                $toCity = $res->result[0];
            } else {
                foreach ($res->result as $r) {
                    if ($r->type == 'Город') {
                        $toCity = $r;
                    }
                }
            }
        }

        if ($toCity) {
            $deliveryServicesArr = array_filter(unserialize($deliveryServices));

            $deliveryServices = array();
            if (!empty($deliveryServicesArr)) {
                foreach ($deliveryServicesArr as $deliveryService) {
                    $deliveryServices[] = array(
                        'id' => $deliveryService,
                    );
                }
            }

            $date = new \DateTime('tomorrow');

            $res = $orderadmin->setRequest(array(
                'delivery-services' => $deliveryServices,
                'from' => array(
                    'warehouse' => $warehouse,
                ),
                'to' => array(
                    "extId" => $toCity->extId,
                ),
                "date" => $date->format('Y-m-d'),
                "weight" => $arParams['CALCULATION']['WEIGHT'] ? $arParams['CALCULATION']['WEIGHT'] : $baseWeight,
                "width" => $arParams['CALCULATION']['WIDTH'],
                "height" => $arParams['CALCULATION']['HEIGHT'],
                "length" => $arParams['CALCULATION']['LENGTH'],
                "payment" => $arParams['CALCULATION']['PAYMENT'],
                "price" => $arParams['CALCULATION']['PRICE']
            ))->request('POST', '/delivery-services/calculator')->getResult();

            $deliveryResult = json_decode($res, true);
        }

        $obCache->EndDataCache(array(
            "PARAMS" => $arParams,
            "RESULT" => $deliveryResult,
        ));
    }
}

$arResult = $deliveryResult;
$arResult['PARAMS'] = $params;

$this->IncludeComponentTemplate();
?>