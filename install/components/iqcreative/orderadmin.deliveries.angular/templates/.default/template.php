<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?
/*
$this->addExternalCss("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css");
$this->addExternalCss("https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css");
$this->addExternalCss("//" . $arResult['SERVER'] . "/frontend/build/orderadmin-calculator/orderadmin-calculator.css");


<script type="text/javascript" src="//<?= $arResult['SERVER']; ?>/frontend/components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="//<?= $arResult['SERVER']; ?>/frontend/components/underscore/underscore.js"></script>
<script type="text/javascript" src="//<?= $arResult['SERVER']; ?>/frontend/components/bootstrap/dist/js/bootstrap.min.js"></script>
*/
?>



<div ng-app="calculator">
    <calculator-directive template-url="<?= str_replace($_SERVER['DOCUMENT_ROOT'], '', __DIR__); ?>/tpl/calculator-directive.html"></calculator-directive>
</div>

<?
/*
<script type="text/javascript">
    window.GLOBAL_CONFIG = {
        server: '/',
        endpoints: {
            calculator: 'bitrix/services/orderadmin/request.php?query=calculator&basket=<?= $arResult['BASKET_ID']; ?>&paysystem=<?= $arResult['PAY_SYSTEM']; ?>',
            locations: 'bitrix/services/orderadmin/request.php?query=localities',
            geolocation: 'bitrix/services/orderadmin/request.php?query=detect',
        },
        settings: {
            sender: 1,
            country: 28,
            city: 62217
        },
        parcel: { },
    };
</script>

<script type="text/javascript" src="//<?= $arResult['SERVER']; ?>/frontend/components/angular/angular.min.js"></script>
<script src="//<?= $arResult['SERVER']; ?>/frontend/libs/ya-map-2.1.js"></script>
<script src="//<?= $arResult['SERVER']; ?>/frontend/components/angular-ymaps/angular-ymaps.js"></script>
<script src="//<?= $arResult['SERVER']; ?>/frontend/components/angularjs-slider/dist/rzslider.min.js"></script>

<script src="//<?= $arResult['SERVER']; ?>/frontend/build/orderadmin-calculator/orderadmin-calculator.js"></script>

*/
?>
