<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

define('MODULE_NAME', 'iqcreative.orderadmin');

$MODULE_ID = "iqcreative.orderadmin";

$arResult = array(
    'SERVER' => COption::GetOptionString($MODULE_ID, 'ORDERADMIN_SERVER'),
    'BASKET_ID' => CSaleBasket::GetBasketUserID(true),
);

$this->IncludeComponentTemplate();
?>