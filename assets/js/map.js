/**
 * Created by tasselchof on 31.03.17.
 */

$(function () {
    function oaLoadMap() {
        var map;

        $.ajax({
            url: '/bitrix/services/orderadmin/index.php?type=calculation',
            cache: true,
            async: false,
            success: function (data) {
                var server = '//panel.orderadmin.ru';

                var mapLayer = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
                    maxZoom: 18,
                    id: 'your.mapbox.project.id',
                    accessToken: 'your.mapbox.public.access.token'
                });

                console.log(data);

                if (data.deliveryServices != undefined) {
                    var deliveryServices = [];
                    var servicePointsLayers = [];
                    var servicePointMarkers = [];
                    $.each(data.deliveryServices, function (index, item) {
                        deliveryServices[item.id] = item;

                        servicePointsLayers[item.id] = L.markerClusterGroup();

                        if (item.logo != null) {
                            servicePointMarkers[item.id] = L.icon({
                                iconUrl: server + item.logo,
                                iconSize: [39],
                                className: 'oaIcon'
                            });
                        } else {
                            servicePointMarkers[item.id] = L.icon();
                        }
                    });

                    var servicePointRates = []
                    $.each(data.rates, function (index, item) {
                        if (
                            item.type == 'service_point' ||
                            item.type == 'self_service_point'
                        ) {
                            if (
                                servicePointRates[item.deliveryService.id] == undefined ||
                                (servicePointRates[item.deliveryService.id] != undefined && item.price < servicePointRates[item.deliveryService.id].price)
                            ) {
                                servicePointRates[item.deliveryService.id] = item;
                            }
                        }
                    });

                    var bounds = [];
                    $.each(data.servicePoints, function (index, item) {
                        if (
                            item.geo.latitude != undefined &&
                            item.geo.longitude != undefined &&
                            servicePointRates[item.deliveryService.id] != undefined
                        ) {
                            var marker = L.marker([item.geo.latitude, item.geo.longitude], {icon: servicePointMarkers[item.deliveryService.id]})
                            //.addTo(servicePointsLayers[item.deliveryService.id])
                                .bindPopup(
                                    '<div class="oaPopup">' +
                                    '<div class="row">' +
                                    '<div class="col-xs-3 oaPopupPrice">' +
                                    (deliveryServices[item.deliveryService.id].logo != undefined ? '<img src="' + server + deliveryServices[item.deliveryService.id].logo + '" class="oaPopupImage"><br />' : '') +
                                    (servicePointRates[item.deliveryService.id] != undefined ? 'Цена: <b>' + servicePointRates[item.deliveryService.id].price + '</b>' : '') +
                                    '<button class="oaPopupButton" data-action="oa-set-service-point-id" data-target="' + item.id + '" data-rate-id="' + servicePointRates[item.deliveryService.id].id + '">Выбрать</button>' +
                                    '</div><div class="col-xs-9">' +
                                    '<h3>' + item.name + '</h3>' +
                                    (item.address ? '<div>Адрес: <b>' + item.address + '</b></div>' : '') +
                                    (item.timetable ? '<div>Время работы: <b>' + item.timetable + '</b></div>' : '') +
                                    (item.phone ? '<div>Телефон: <b>' + item.phone + '</b></div>' : '') +
                                    (item.extId ? '<div>Код ПВЗ: <b>' + item.extId + '</b></div>' : '') +
                                    (item.description ? '<div>' + item.description + '</div>' : '') +
                                    '</div></div></div>', {minWidth: 425}
                                );

                            servicePointsLayers[item.deliveryService.id].addLayer(marker);

                            bounds.push([item.geo.latitude, item.geo.longitude]);
                        }
                    });

                    var mapLayers = [mapLayer];
                    map = L.map('oaServicePointsMap', {
                        center: [51.505, -0.09],
                        zoom: 10,
                        layers: mapLayers
                    });

                    map.fitBounds(bounds);

                    var overlays = {};
                    $.each(data.deliveryServices, function (index, item) {
                        var length = servicePointsLayers[item.id].getLayers().length;

                        if (length > 0) {
                            var name = item.name + ' (' + length + ')';

                            overlays[name] = servicePointsLayers[item.id];

                            map.addLayer(servicePointsLayers[item.id]);
                        }
                    });

                    L.control.layers({}, overlays).addTo(map);
                }
            }
        });

        return map;
    }

    var map;
    $('body').on('shown.bs.modal', '#oaServicePointsModal', function (e) {
        map = oaLoadMap();
    });

    $('body').on('hide.bs.modal', '#oaServicePointsModal', function (e) {
        map.remove();
    });

    var rateId;
    $('body').on('click', '[data-action="oa-set-service-point-id"]', function (e) {
        var rateId = $(this).data('rate-id');
        var servicePointId = $(this).data('target');

        if (oaServicePointProp != false) {
            $('input[name="ORDER_PROP_' + oaServicePointProp.ID + '"]').val(servicePointId);

            $('input[name="ORDER_PROP_' + oaServicePointProp.ID + '"]').attr('readonly', 'readonly');
        } else {
            console.log('Service point property not found');
        }

        if (oaRates[rateId] != undefined) {
            var checkbox = $('input[type="checkbox"][name="DELIVERY_ID"][value="' + oaRates[rateId] + '"]');
            if (!checkbox.checked) {
                checkbox.trigger('click');
            }
        }

        $('#oaServicePointsModal').modal('toggle');
    });

    $('body').on('click', 'input[type="checkbox"][name="DELIVERY_ID"]', function (e) {
        if (oaServicePointProp != false) {
            $('input[name="ORDER_PROP_' + oaServicePointProp.ID + '"]').val('');
            $('input[name="ORDER_PROP_' + oaServicePointProp.ID + '"]').attr('readonly', true);
        } else {
            console.log('Service point property not found');
        }

        if ($(this).checked) {
            rateId = $(this).val();

            console.log(rateId);
        }
    });

    $('input[name="ORDER_PROP_' + oaServicePointProp.ID + '"]').attr('placeholder', 'Выбрать...');
    $('input[name="ORDER_PROP_' + oaServicePointProp.ID + '"]').attr('data-action', 'oa-set-service-point-id');

    $('#bx-soa-delivery .bx-soa-section-title-container > h2').append('<a href="#" role="button" class="btn" data-action="oa-load-map" data-toggle="modal" data-target="#oaServicePointsModal" style="padding: 0 0 0 15px;">Выбрать на карте</a>');
});