<?php
/**
 * Created by PhpStorm.
 * User: tasselchof
 * Date: 13.09.15
 * Time: 15:20
 */

namespace Bitrix\Orderadmin;

use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Data\Cache;
use Bitrix\Main\Web\HttpClient;
use Bitrix\Sale\BasketItem;

if (!Loader::includeModule('catalog') || !Loader::includeModule('sale')) {
    return;
}

class Api
{
    static $MODULE_ID = "iqcreative.orderadmin";
    static $MODULE_VERSION = '0.1.7';
    static $MODULE_CACHE_PERIOD = 86400;

    protected $publicKey;
    protected $secret;
    protected $key;
    protected $json;
    protected $result;
    protected $timeout = 45;
    protected $error;

    static function checkAuthorization($key)
    {
        $secret = Option::get(self::$MODULE_ID, 'ORDERADMIN_SECRET');

        if (hash_hmac('sha1', gmdate('Y-m-d'), $secret) == $key) {
            return true;
        }

        CHTTP::SetStatus("403 Forbidden");
        die('{"error":"Authorization failed"}');
    }

    public function __construct($publicKey = null, $secret = null)
    {
        $this->publicKey = $publicKey;
        $this->secret = $secret;
        $this->key = hash_hmac('sha1', date('Y-m-d'), $secret);
    }

    public function getResult($decode = false)
    {
        if ($decode) {
            return json_decode($this->result, true);
        }

        return $this->result;
    }

    public function getError($decode = false)
    {
        if ($decode) {
            return json_decode($this->error, true);
        }

        return $this->error;
    }

    public function setRequest($array)
    {
        $this->json = json_encode($array);

        return $this;
    }

    public function setJsonRequest($json)
    {
        $this->json = $json;

        return $this;
    }

    public function getRequest()
    {
        return $this->json;
    }

    public function request($type, $url, $query = array(), $debug = false,
        $cacheResponse = true
    ) {
        if (!$debug && in_array($type, array('GET'))) {
            $cacheKeys = array(
                'MODULE'  => self::$MODULE_ID,
                'VERSION' => self::$MODULE_VERSION,
                'URL'     => $type,
                'NAME'    => $url,
            );

            if (!empty($query)) {
                $cacheKeys = array_merge($cacheKeys, $query);
            }

//            \Bitrix\Main\Diag\Debug::dumpToFile(
//                $cacheKeys, '', '__bx_cache_log.log'
//            );

            $obCache = Cache::createInstance();
            $cache = $obCache->initCache(
                self::$MODULE_CACHE_PERIOD, md5(json_encode($cacheKeys)), '/'
            );
        }

        if (!$debug && $cache) {
            $vars = $obCache->getVars();

            if (empty($vars['RESULT'])) {
                $cache = false;
            }
        }

        if ($cacheResponse && $cache) {
            $this->result = $vars["RESULT"];

            $result = $this->getResult(true);

            if (isset($result['total_items'])
                && empty($result['total_items'])
            ) {
                self::request($type, $url, $query, $debug, false);
            }
        } else {
            $httpClient = new \Bitrix\Main\Web\HttpClient();
            $httpClient->setStreamTimeout($this->timeout);
            $httpClient->setRedirect(false);
            $httpClient->setHeader('Content-Type', 'application/json');

            $endpoint = Option::get(self::$MODULE_ID, 'ORDERADMIN_API_URL');
            if (strpos($url, '/api/') !== false) {
                $endpoint = str_replace('/api/rest/latest', '', $endpoint);

                $httpClient->setAuthorization($this->publicKey, $this->secret);
            } else {
                $httpClient->setHeader(
                    'Authorization', 'OAD ' . join(
                        ':', array($this->publicKey, $this->key, date('Y-m-d'))
                    )
                );
            }

            $url = $endpoint . $url . '?' . http_build_query($query);

            $post = $this->getRequest();

            $httpClient->query($type, $url, $post);

            if ($debug) { //  && strpos($url, 'calculator') !== false
                echo "<pre>";
                echo "<b>" . __FILE__ . "</b><br/>";
                var_dump($url);
                var_dump($post);
                var_dump($httpClient);
                var_dump($httpClient->getError());
                var_dump($httpClient->getHeaders()->toString());
                var_dump($httpClient->getStatus());
                var_dump($httpClient->getResult());
                var_dump(json_decode($httpClient->getResult()));
                echo "</pre>";
                die();
            }

            // Trash, that need to be corrected
            if (in_array($httpClient->getStatus(), [200, 201, 302])) {
                $this->result = $httpClient->getResult();

                if (in_array($type, array(HttpClient::HTTP_GET))
                    && !empty($this->result)
                    && $obCache->startDataCache()
                ) {
                    $result = $this->getResult(true);

                    if (!isset($result['total_items'])
                        || $result['total_items'] > 0
                    ) {
                        $obCache->endDataCache(
                            array(
                                "RESULT" => $this->result,
                            )
                        );
                    }
                }
            } else {
                $this->error = $httpClient->getResult();
                $this->result = false;

                if (in_array($type, array(HttpClient::HTTP_GET))
                    && !empty($this->result)
                    && $obCache->startDataCache()
                ) {
                    $obCache->endDataCache(
                        array(
                            "RESULT" => null,
                        )
                    );
                }
            }
        }

        return $this;
    }

    public function updateItemsStats($debug = false)
    {
        $publicKey = Option::get(self::$MODULE_ID, 'ORDERADMIN_PUBLIC_KEY');
        $secret = Option::get(self::$MODULE_ID, 'ORDERADMIN_SECRET');

        $updateQuantity = Option::get(
            self::$MODULE_ID, 'ORDERADMIN_AGENTS_QUANTITY'
        );
        $updateWeight = Option::get(
            self::$MODULE_ID, 'ORDERADMIN_AGENTS_WEIGHT'
        );
        $updateVolume = Option::get(
            self::$MODULE_ID, 'ORDERADMIN_AGENTS_VOLUME'
        );

        $storageApiUrl = Option::get(
            self::$MODULE_ID, 'ORDERADMIN_API_STORAGE_URL'
        );

        $orderadmin = new \Bitrix\Orderadmin\Api($publicKey, $secret);

        $items = $orderadmin->request(
            'GET',
            '/storage/' . Option::get(self::$MODULE_ID, 'ORDERADMIN_WAREHOUSE')
            . '/product-options/items'
        )->getResult();

        if (!empty($storageApiUrl)) {
            $items = file_get_contents($storageApiUrl);
        }

        if ($items) {
            $items = json_decode($items);
            $items = $items->result;

            foreach ($items as $itemId => $value) {
                $update = array();

                if ($updateQuantity) {
                    switch ($value->state) {
                        case 'normal':
                            $update['QUANTITY'] = $value->count;
                            break;

                        case 'blocked':
                            $update['QUANTITY_RESERVED'] = $value->count;
                            break;
                    }
                }

                if ($updateWeight) {
                    $update['WEIGHT'] = $value->maxWeight;
                }

                if ($updateVolume) {
                    $update['WIDTH']
                        = $update['LENGTH']
                        =
                    $update['HEIGHT'] = ceil(pow($value->maxVolume, 1 / 3));
                }

                if (!empty($update)) {
                    $product = CIBlockElement::GetById($value->extId)->Fetch();

                    echo '<p>Updating product ' . $value->extId . ' (<b>'
                        . $product['NAME'] . '</b>): ' . var_export(
                            $update, true
                        ) . '</p>';

                    CCatalogProduct::Update($value->extId, $update);

                    if ($_REQUEST['debug']) {
                        echo '<p>' . $value->extId . '</p>';
                        echo '<pre>';
                        print_r($value);
                        print_r($update);
                        echo '</pre>';
                    }
                }
            }
        }
    }

    public function createDeliveryRequest($orderId)
    {
        $senderId = Option::get(self::$MODULE_ID, 'ORDERADMIN_SENDER');
        $countryId = Option::get(self::$MODULE_ID, 'ORDERADMIN_BASE_COUNTRY');
        $dsDefaultRateId = Option::get(
            self::$MODULE_ID, 'ORDERADMIN_DELIVERY_SERVICES_DEFAULT_RATE'
        );

        $order = \Bitrix\Sale\Order::load($orderId);

        $basket = $order->getBasket();

        $propertyCollection = $order->getPropertyCollection();
        $paymentCollection = $order->getPaymentCollection();

        $user = \Bitrix\Main\UserTable::getById($order->getUserId())->Fetch();

        $properties = array();
        /** @var \Bitrix\Sale\PropertyValue $property */
        foreach ($propertyCollection as $property) {
            if ($property->getType() == 'ENUM') {
                $properties[$property->getName()] = $property->getProperty(
                )['OPTIONS'][$property->getValue()];
            } else {
                $properties[$property->getName()] = $property->getValue();
            }
        }

        $recipientPhone = $user['PERSONAL_MOBILE'];
        if (empty($recipientPhone)) {
            $recipientPhone = $user['PERSONAL_PHONE'];
        }

        $data = array(
//            'state' => 'sent',
'sender'            => $senderId,
'senderAddress'     => 1058909,
'rate'              => $dsDefaultRateId,
'extId'             => $order->getId(),
'estimatedCost'     => $order->getPrice(),
'payment'           => $paymentCollection->isPaid() ? 0
    : $order->getPaymentCollection()->getSum(),
'recipientPhone'    => $recipientPhone,
'recipientLocality' => array(
    'country' => $countryId,
    'name'    => 'Варна',
),
'recipient'         => array(
    //'extId' => $propertyCollection->getProfileId(),
    'name'  => join(
        ' ', array_filter(array($user['NAME'], $user['LAST_NAME']))
    ),
    'phone' => $recipientPhone,
    'email' => $user['EMAIL'],
),
'recipientAddress'  => array(
    'locality'  => array(
        'country' => $countryId,
        'name'    => 'Варна',
    ),
    'postcode'  => $propertyCollection->getDeliveryLocationZip(),
    'quarter'   => $properties['Квартал'],
    'notFormal' => $properties['Адрес'],
),
        );

        $basketItems = $basket->getBasketItems();

        $place = array(
            'items' => array(),
        );

        /** @var BasketItem $basketItem */
        foreach ($basketItems as $basketItem) {
            $place['items'][] = array(
                'extId'         => $basketItem->getProductId(),
                'name'          => $basketItem->getField('NAME'),
                'price'         => $basketItem->getPrice(),
                'tax'           => $basketItem->getVat(),
                'count'         => $basketItem->getQuantity(),
                'estimatedCost' => $basketItem->getQuantity()
                    * $basketItem->getPrice(),
                'payment'       => $paymentCollection->isPaid() ? 0
                    : ($basketItem->getQuantity() * $basketItem->getPrice()),
            );
        }

        $data['places'] = array($place);

        $res = $this->setRequest($data)->request(
            HttpClient::HTTP_POST, '/api/delivery-services/requests'
        )->getResult(true);
        if (!$res) {
            throw new \Exception($this->getError(true)['detail']);
        } else {
            $this->bitrixUpdateRequest(
                $orderId, '/api/delivery-services/requests', array(
                'request'  => "'" . addslashes(json_encode($this->getRequest()))
                    . "'",
                'response' => "'" . addslashes(json_encode($res)) . "'",
            )
            );
        }

        return $res;
    }

    public function createDeliveryTask($orderId, $deliveryRequestId, $executive)
    {
        $data = array(
            'queue'           => 1,
            'state'           => 'assigned',
            'deliveryRequest' => $deliveryRequestId,
            'executive'       => $executive,
        );

        $res = $this->setRequest($data)->request(
            HttpClient::HTTP_POST, '/api/delivery-services/delivery/task'
        )->getResult(true);
        if (!$res) {
            throw new \Exception($this->getError(true)['detail']);
        } else {
            $this->bitrixUpdateRequest(
                $orderId, '/api/delivery-services/delivery/task', array(
                'request'  => "'" . addslashes(json_encode($this->getRequest()))
                    . "'",
                'response' => "'" . addslashes(json_encode($res)) . "'",
            )
            );
        }

        return $res;
    }

    public function bitrixGetRequest($orderId, $endpoint)
    {
        global $DB;

        $results = $DB->Query(
            "SELECT * FROM `b_orderadmin_requests` WHERE `order_id`='"
            . $orderId . "' AND endpoint = '" . $endpoint . "'"
        );

        return $results->Fetch();
    }

    public function bitrixUpdateRequest($orderId, $endpoint, $update)
    {
        global $DB;

        $results = $DB->Query(
            "SELECT * FROM `b_orderadmin_requests` WHERE `order_id`='"
            . $orderId . "' AND endpoint = '" . $endpoint . "'"
        );

        $orderadminRequest = $results->Fetch();

        if (isset($orderadminRequest['basket_id'])) {
            $DB->Update(
                "b_orderadmin_requests", $update,
                "WHERE order_id='" . $orderId . "'"
            );
        } else {
            $update['order_id'] = $orderId;
            $update['endpoint'] = "'$endpoint'";

            $DB->Insert("b_orderadmin_requests", $update);
        }
    }

    public function createOrder($orderId)
    {
        $publicKey = Option::get(self::$MODULE_ID, 'ORDERADMIN_PUBLIC_KEY');
        $secret = Option::get(self::$MODULE_ID, 'ORDERADMIN_SECRET');

        $shopId = Option::get(self::$MODULE_ID, 'ORDERADMIN_SHOP');
        $countryId = Option::get(self::$MODULE_ID, 'ORDERADMIN_BASE_COUNTRY');

        $orderadmin = new \Bitrix\Orderadmin\Api($publicKey, $secret);

        $order = \Bitrix\Sale\Order::load($orderId);

        $basket = $order->getBasket();

        $propertyCollection = $order->getPropertyCollection();
        $paymentCollection = $order->getPaymentCollection();

        $user = \Bitrix\Main\UserTable::getById($order->getUserId())->Fetch();

        $properties = array();
        /** @var \Bitrix\Sale\PropertyValue $property */
        foreach ($propertyCollection as $property) {
            if ($property->getType() == 'ENUM') {
                $properties[$property->getName()] = $property->getProperty(
                )['OPTIONS'][$property->getValue()];
            } else {
                $properties[$property->getName()] = $property->getValue();
            }
        }

        $data = array(
            'shop'         => $shopId,
            'extId'        => $order->getId(),
            'date'         => $order->getDateInsert()->toString(),
            'paymentState' => $paymentCollection->isPaid() ? 'paid'
                : 'not_paid',
            'orderPrice'   => $basket->getBasePrice(),
            'totalPrice'   => $order->getPrice(),
            'profile'      => array(
                //'extId' => $propertyCollection->getProfileId(),
                'name'  => join(
                    ' ', array_filter(array($user['NAME'], $user['LAST_NAME']))
                ),
                'phone' => $user['PERSONAL_MOBILE'],
                'email' => $user['EMAIL'],
            ),
            'address'      => array(
                'locality'  => array(
                    'country' => $countryId,
                    'name'    => 'Варна',
                ),
                'postcode'  => $propertyCollection->getDeliveryLocationZip(),
                'quarter'   => $properties['Квартал'],
                'notFormal' => $properties['Адрес'],
            ),
        );

        $basketItems = $basket->getBasketItems();

        /** @var BasketItem $basketItem */
        foreach ($basketItems as $basketItem) {
            $orderProduct = array(
                'productOffer' => array(
                    'shop'  => $data['shop'],
                    'extId' => $basketItem->getProductId(),
                ),
                'price'        => $basketItem->getPrice(),
                'tax'          => $basketItem->getVat(),
                'count'        => $basketItem->getQuantity(),
                'total'        => $basketItem->getFinalPrice(),
            );

            $data['orderProducts'] = array(
                $orderProduct,
            );
        }

        foreach ($data['orderProducts'] as $orderProduct) {
            $data['orderPrice'] += $orderProduct['total'];
        }

        $res = $orderadmin->setRequest($data)->request(
            HttpClient::HTTP_POST, '/api/products/order'
        )->getResult();
        if (!$res) {
            echo "<pre>";
            echo "<b>" . __FILE__ . "</b><br/>";
            var_dump($orderadmin->getError(true));
            echo "</pre>";
            die();
        }
    }

    public function createDeliveryRequests()
    {
        $orders = \Bitrix\Sale\Order::getList(
            array(
                'filter' => array(
                    'ID' => 16688,
                ),
                'order'  => array(
                    'ID' => 'DESC',
                ),
            )
        );
        while ($order = $orders->Fetch()) {
            self::createDeliveryRequest($order['ID']);

            die();
        }
    }

    public function createOrders()
    {
        $orders = \Bitrix\Sale\Order::getList(
            array(
                'order' => array(
                    'ID' => 'DESC',
                ),
            )
        );
        while ($order = $orders->Fetch()) {
            self::createOrder($order['ID']);

            die();
        }
    }

    public function getCalculationResult()
    {
        global $DB;

        $basketId = \CSaleBasket::GetBasketUserID();

        $results = $DB->Query(
            "SELECT * FROM `b_orderadmin_requests` WHERE `basket_id`='"
            . $basketId . "'"
        );

        $orderadminRequest = $results->Fetch();

        if (isset($orderadminRequest['request'])
            && $orderadminRequest['request'] == $this->getRequest()
        ) {
            return $orderadminRequest;
        } else {
            return false;
        }
    }

    public function saveCalculationResult()
    {
        global $DB;

        $basketId = \CSaleBasket::GetBasketUserID();

        $results = $DB->Query(
            "SELECT * FROM `b_orderadmin_requests` WHERE `basket_id`='"
            . $basketId . "'"
        );

        $orderadminRequest = $results->Fetch();

        $arFields = array(
            "request"  => "'" . $this->getRequest() . "'",
            "response" => "'" . $this->getResult() . "'",
        );

        if (isset($orderadminRequest['basket_id'])) {
            $DB->Update(
                "b_orderadmin_requests", $arFields,
                "WHERE basket_id='" . $basketId . "'"
            );
        } else {
            $arFields['basket_id'] = $basketId;

            $DB->Insert("b_orderadmin_requests", $arFields, '', true);
        }
    }
}